import React, { FC } from "react";
import styled from "@emotion/styled";
import Link from "next/link";

import {
  motion,
  AnimatePresence,
  useViewportScroll,
  useSpring,
  useTransform,
} from "framer-motion";

import { IMovieResult, ITVResult } from "../../API_CALLS/movies.types";

import { getBackgroundWallpaperPath } from "../../utils/imageUtils";
import { getDate } from "../../utils/getDate";

interface HeroDisplayProps {
  selectedMovieItem: IMovieResult | null;
  selectedTVItem: ITVResult | null;
}

const HeroDisplay: FC<HeroDisplayProps> = ({
  selectedMovieItem,
  selectedTVItem,
}) => {
  const { scrollY } = useViewportScroll();
  const elasticParallax = useSpring(
    useTransform(scrollY, [0, 900], ["0%", "200%"]),
    { stiffness: 90, damping: 20 }
  );

  return (
    <StyledSection>
      <AnimatePresence initial={false} exitBeforeEnter>
        {!selectedMovieItem && !selectedTVItem && (
          <HeroContainer
            backdropPath={"/images/starWars.jpg"}
            variants={{
              animate: { opacity: 1 },
              hidden: { opacity: 0 },
            }}
            animate="animate"
            initial="hidden"
            exit="hidden"
          >
            <ParallaxContainer
              initial={{ y: 0 }}
              style={{ y: elasticParallax }}
            >
              <h1>The Movie Database</h1>
              <p>Search through movie database application.</p>
              <p>
                Find out what is currently in cinemas, most popular among users
                or has high rating
              </p>
            </ParallaxContainer>
          </HeroContainer>
        )}
      </AnimatePresence>
      <AnimatePresence key={selectedMovieItem?.id} exitBeforeEnter>
        {selectedMovieItem && (
          <HeroContainer
            backdropPath={
              getBackgroundWallpaperPath(selectedMovieItem.backdrop_path) ??
              "/images/starWars.jpg"
            }
            variants={{
              animate: {
                opacity: 1,
                transition: { delay: 0.15 },
              },
              hidden: { opacity: 0 },
            }}
            animate="animate"
            initial="hidden"
            exit="hidden"
          >
            <SelectedItemSection
              initial={{ y: 0 }}
              style={{ y: elasticParallax }}
            >
              <TitleInfo>
                <h2>{selectedMovieItem.title}</h2>
                {selectedMovieItem.release_date && (
                  <div>
                    release date: {getDate(selectedMovieItem.release_date)}
                  </div>
                )}
              </TitleInfo>
              <SelectedItemInfo>{selectedMovieItem.overview}</SelectedItemInfo>
              <SelectButton whileHover={{ scale: 1.1 }}>
                <Link href={`/movie/${selectedMovieItem.id}`}>
                  <a>See More</a>
                </Link>
              </SelectButton>
            </SelectedItemSection>
          </HeroContainer>
        )}
      </AnimatePresence>
      <AnimatePresence key={selectedTVItem?.id} exitBeforeEnter>
        {selectedTVItem && (
          <HeroContainer
            backdropPath={
              getBackgroundWallpaperPath(selectedTVItem.backdrop_path) ??
              "/images/starWars.jpg"
            }
            variants={{
              animate: {
                opacity: 1,
                transition: { delay: 0.15 },
              },
              hidden: { opacity: 0 },
            }}
            animate="animate"
            initial="hidden"
            exit="hidden"
          >
            <SelectedItemSection
              initial={{ y: 0 }}
              style={{ y: elasticParallax }}
            >
              <TitleInfo>
                <h2>{selectedTVItem.name}</h2>

                {selectedTVItem.first_air_date && (
                  <div>
                    release date: {getDate(selectedTVItem.first_air_date)}
                  </div>
                )}
              </TitleInfo>
              <SelectedItemInfo>{selectedTVItem.overview}</SelectedItemInfo>
              <SelectButton whileHover={{ scale: 1.1 }}>
                <Link href={`/tv/${selectedTVItem.id}`}>
                  <a>See More</a>
                </Link>
              </SelectButton>
            </SelectedItemSection>
          </HeroContainer>
        )}
      </AnimatePresence>
    </StyledSection>
  );
};

export default HeroDisplay;

interface HeroContainerProps {
  backdropPath: string | null;
}

const StyledSection = styled.section`
  position: relative;
  height: fit-content;
  min-height: 60vh;

  @media (max-width: ${({ theme }) => theme.breakPoints.hero.small}) {
    min-height: 50rem;
    padding: 1rem 0;
  }
`;

const HeroContainer = styled(motion.div)<HeroContainerProps>`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  min-height: 60vh;
  overflow: hidden;
  background: linear-gradient(rgba(0, 0, 0, 0.75), rgb(5, 12, 24)),
    url('${({ backdropPath }) => backdropPath}')
      center/cover;
  display: flex;
  align-items: center;

 @media (max-width: ${({ theme }) => theme.breakPoints.hero.small}) {
     align-items: flex-start;
     min-height: 50rem;
     padding-bottom: 1.8rem;
    }
`;

const ParallaxContainer = styled(motion.div)`
  margin-left: auto;
  margin-right: 15rem;
  max-width: 30rem;

  @media (max-width: 580px) {
    margin: 2rem auto;
  }

  h1 {
    font-size: 3rem;
    border-top: 2px solid white;
    padding-top: 1rem;
    font-family: ${({ theme }) => theme.fonts.Oxygen};

    @media (max-width: 580px) {
      font-size: 2.5rem;
      border-top: none;
    }
  }
`;

const TitleInfo = styled.header`
  display: flex;
  width: 100%;
  align-items: flex-end;
  justify-content: space-between;
  padding: 1rem;
  max-width: 120rem;
  margin: 0;

  @media (max-width: 580px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  h2 {
    margin: 0 2rem 0 0;
    max-width: 25rem;
    @media (max-width: 580px) {
      text-align: center;
      margin-bottom: 1.5rem;
    }
  }
`;
const SelectedItemSection = styled(motion.section)`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const SelectedItemInfo = styled.p`
  max-width: 30rem;
  text-align: center;
`;

const SelectButton = styled(motion.button)`
  background: ${({ theme }) => theme.colors.secondaryBlueTransparent};
  border: 1px solid ${({ theme }) => theme.colors.secondaryBlue};
  border-radius: 2px;
  color: white;
  padding: 0.5rem 1rem;
`;
