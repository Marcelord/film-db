import React, { useCallback, useEffect, FC } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from '@emotion/styled';

import CardCarousel from '../carousel/CardCarousel';
import SelectButtons from '../SelectButtons/SelectButtons';
import SimpleCardWithRating from '../cards/SimpleCardWithRating';

import {
    fetchHeroMovieList,
    fetchHeroTVList,
    setHeroTVItem,
    setHeroMovieItem,
    fetchHeroMovieListSuccess,
} from '../../redux/hero/hero.actions';
import { IReduxStore } from '../../redux/store';
import { activeListType } from '../../redux/hero/hero.reducer';

import { getPosterImagePath } from '../../utils/imageUtils';
import HeroDisplay from './HeroDisplay';
import { IMovieResult } from '../../API_CALLS/movies.types';

interface HeroProps {
    initialHeroList: IMovieResult[];
}

const HeroComponent: FC<HeroProps> = ({ initialHeroList }) => {
    const dispatch = useDispatch();
    const heroMovieList = useSelector(
        (state: IReduxStore) => state.hero.heroMovieList
    );
    const heroTVList = useSelector(
        (state: IReduxStore) => state.hero.heroTVList
    );
    const activeListType: activeListType = useSelector(
        (state: IReduxStore) => state.hero.activeList
    );

    const selectedMovieItem = useSelector(
        (state: IReduxStore) => state.hero.selectedMovieHero
    );

    const selectedTVItem = useSelector(
        (state: IReduxStore) => state.hero.selectedTVHero
    );


    // silent dispatch on mount / if there were issue on static props there will be another fetch attempt
    useEffect(() => {
        if (!(heroMovieList.length > 0) && !(initialHeroList.length > 0)) {
            dispatch(fetchHeroMovieList());

            return;
        }

        if(initialHeroList.length > 0) {
            dispatch(fetchHeroMovieListSuccess(initialHeroList))
        }
    }, []);

    const handleSelect = useCallback(
        (id: number | string) => {
            activeListType === 'movie'
                ? dispatch(setHeroMovieItem(id))
                : dispatch(setHeroTVItem(id));
        },
        [activeListType]
    );

    return (
        <div>
            <HeroDisplay
                selectedMovieItem={
                    activeListType === 'movie' ? selectedMovieItem : null
                }
                selectedTVItem={activeListType === 'tv' ? selectedTVItem : null}
            />
            <PopularText>What is popular</PopularText>
            <CardCarousel arrowsPosition="center">
                {activeListType === 'movie' &&
                    heroMovieList.length === 0 &&
                    initialHeroList?.map((heroItem) => {
                        return (
                            <SimpleCardWithRating
                                key={heroItem.id}
                                imagePath={getPosterImagePath(
                                    heroItem.poster_path
                                )}
                                rating={heroItem.vote_average}
                                title={heroItem.title}
                                onSelect={() => {
                                    handleSelect(heroItem.id);
                                }}
                            />
                        );
                    })}
                {activeListType === 'movie' &&
                    heroMovieList?.map((heroItem) => {
                        return (
                            <SimpleCardWithRating
                                key={heroItem.id}
                                imagePath={getPosterImagePath(
                                    heroItem.poster_path
                                )}
                                rating={heroItem.vote_average}
                                title={heroItem.title}
                                onSelect={() => {
                                    handleSelect(heroItem.id);
                                }}
                            />
                        );
                    })}
                {activeListType === 'tv' &&
                    heroTVList?.map((heroItem) => {
                        return (
                            <SimpleCardWithRating
                                key={heroItem.id}
                                imagePath={getPosterImagePath(
                                    heroItem.poster_path
                                )}
                                rating={heroItem.vote_average}
                                title={heroItem.name}
                                onSelect={() => {
                                    handleSelect(heroItem.id);
                                }}
                            />
                        );
                    })}
            </CardCarousel>

            <SelectButtons
                firstOptionTitle={'movie'}
                secondOptionTitle={'Tv'}
                firstOptionSelect={() => {
                    dispatch(fetchHeroMovieList());
                }}
                secondOptionSelect={() => {
                    dispatch(fetchHeroTVList());
                }}
                isOverline
            />
        </div>
    );
};

export default HeroComponent;

const PopularText = styled.h4`
    padding: 0.5rem 1rem;
    margin: 2rem 0;
    border-right: 1px solid white;
    display: inline-block;
    font-family: ${({ theme }) => theme.fonts.Montserrat};
`;
