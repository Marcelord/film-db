import React, { useState, FC } from "react";
import styled from "@emotion/styled";
import { motion, AnimateSharedLayout } from "framer-motion";

type selectedButtonType = "first" | "second";
interface selectButtonsProps {
  firstOptionTitle: string;
  secondOptionTitle: string;
  isOverline?: boolean;
  isLightThemed?: boolean;
  firstOptionSelect: () => void;
  secondOptionSelect: () => void;
}

/**
 * @param  firstOptionTitle: string
 * @param secondOptionTitle: string
 * @param isOverline?: boolean
 * @param firstOptionSelect: () => void
 * @param secondOptionSelect: () => void
 * @Description 
 * renders line with 2 buttons appeared as gradient text
 * based on option will button switch his under/over line dynamically
 * 
 * @example <SelectButtons
               firstOptionTitle={"movie"}
               secondOptionTitle={"Tv"}
               firstOptionSelect={() => {
                dispatch(fetchHeroMovieList())
                 }}
               secondOptionSelect={() => {
                dispatch(fetchHeroTVList())
               }}
        /> 
 */

const SelectButtons: FC<selectButtonsProps> = ({
  firstOptionSelect,
  secondOptionSelect,
  firstOptionTitle,
  secondOptionTitle,
  isOverline = false,
  isLightThemed = false,
}) => {
  const [activeButton, setActiveButton] = useState<selectedButtonType>("first");

  const handleFirstOption = () => {
    setActiveButton("first");
    firstOptionSelect();
  };

  const handleSecondOption = () => {
    setActiveButton("second");
    secondOptionSelect();
  };

  return (
    <ButtonsWrapper>
      <AnimateSharedLayout transition={{ type: "spring" }} type="crossfade">
        <ButtonContainer isLight={isLightThemed}>
          {activeButton === "first" && isOverline && (
            <motion.div
              key={"second"}
              layoutId="overline"
              className="overline first"
            />
          )}
          <CoolButton isLight={isLightThemed} onClick={handleFirstOption}>
            <h3>{firstOptionTitle}</h3>
          </CoolButton>
          {activeButton === "first" && !isOverline && (
            <motion.div
              key={"first"}
              layoutId="underline"
              className="underline first"
            />
          )}
        </ButtonContainer>
        <ButtonContainer isLight={isLightThemed}>
          {activeButton === "second" && isOverline && (
            <motion.div
              key={"second"}
              layoutId="overline"
              className="overline second"
            />
          )}
          <CoolButton isLight={isLightThemed} onClick={handleSecondOption}>
            <h3>{secondOptionTitle}</h3>
          </CoolButton>
          {activeButton === "second" && !isOverline && (
            <motion.div
              key={"second"}
              layoutId="underline"
              className="underline second"
            />
          )}
        </ButtonContainer>
      </AnimateSharedLayout>
    </ButtonsWrapper>
  );
};

export default SelectButtons;

const ButtonsWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 1rem auto 2rem;
  overflow: hidden;
`;

const ButtonContainer = styled(motion.div)<{ isLight?: boolean }>`
  margin: 0 2rem;

  .underline,
  .overline {
    width: 100%;
    height: ${({ isLight }) => (isLight ? "2px" : "4px")};

    left: 0;
    border-radius: 10px;
    background: linear-gradient(
      90deg,
      ${({ theme }) => theme.colors.secondaryBlue},
      ${({ theme }) => theme.colors.secondaryTeal}
    );
  }
`;

const CoolButton = styled.button<{ isLight?: boolean }>`
  border: none;
  background: none;

  font-family: ${({ theme }) => theme.fonts.Oxygen};

  cursor: pointer;
  text-transform: capitalize;

  h3 {
    font-size: 2.2rem;
    font-weight: ${({ isLight }) => (isLight ? "400" : "700")};
    background-image: linear-gradient(
      90deg,
      ${({ theme }) => theme.colors.secondaryBlue},
      ${({ theme }) => theme.colors.secondaryTeal}
    );
    background-clip: text;
    -webkit-background-clip: text;
    -moz-background-clip: text;
    color: transparent;
    margin: 0;
  }
`;
