import React, { FC } from "react";
import {
  IRecommendedResult,
  MediumType,
} from "../../../API_CALLS/movies.types";
import styled from "@emotion/styled";
import SimpleCard from "../../cards/SimpleCard";
import { useRouter } from "next/router";

interface DetailRecommendedProps {
  recommendations: IRecommendedResult[];
  mediumType: MediumType;
}

const DetailRecommended: FC<DetailRecommendedProps> = ({
  recommendations,
  mediumType,
}) => {
  const router = useRouter();

  const handleRedirectOnClick = (id: number) => {
    router.push(`/${mediumType}/${id}`);
  };

  return (
    <>
      <Heading>Recommended to Watch</Heading>
      <RecommendationsContainer>
        {recommendations &&
          recommendations.map((recommended) => {
            const title = recommended.name || recommended.title;
            return (
              <SimpleCard
                key={recommended.id}
                title={title}
                posterPath={recommended.poster_path}
                onClick={() => {
                  handleRedirectOnClick(recommended.id);
                }}
              />
            );
          })}
      </RecommendationsContainer>
    </>
  );
};

export default DetailRecommended;

const RecommendationsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: 0rem 2rem 5rem;
  cursor: pointer;
  justify-content: center;
`;

const Heading = styled.h3`
  margin: 6rem 2rem 2rem;
`;
