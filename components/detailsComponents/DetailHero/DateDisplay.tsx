import React, { FC } from 'react';
import styled from '@emotion/styled';

interface DateDisplayProps {
    dateToDisplay: string;
    heading: string;
}

const DateDisplay: FC<DateDisplayProps> = ({ dateToDisplay, heading }) => {
    const getMediumFirstDate = () => {
        return new Date(dateToDisplay).toLocaleDateString('en-GB', {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
        });
    };

    return (
        <DateContainer>
            <ItalicHeading>{heading}</ItalicHeading>
            <span>{getMediumFirstDate()}</span>
        </DateContainer>
    );
};

export default DateDisplay;

const ItalicHeading = styled.h3`
    font-family: ${({ theme }) => theme.fonts.Montserrat};
    font-weight: 400;
    font-style: italic;
    font-size: 1.6rem;
`;

const DateContainer = styled.div`
    margin: 2rem 0 4rem;

    display: flex;
    max-width: 45rem;
    align-items: center;
    justify-content: space-between;
`;
