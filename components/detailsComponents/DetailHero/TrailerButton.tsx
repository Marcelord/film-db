import React, { FC } from 'react';
import { motion } from 'framer-motion';
import styled from '@emotion/styled';
import PlayIcon from '../../icons/Play';

interface TrailerButtonProps {
    onClick?: () => void;
}

const TrailerButton: FC<TrailerButtonProps> = ({ onClick }) => {
    const handleClick = () => {
        onClick?.();
    };

    return (
        <TrailerButtonWrapper
            whileHover={{ width: '16rem' }}
            onClick={handleClick}
            layoutId="native-expand"
            animate
        >
            <AnimatedButtonContainer
                initial={{ opacity: 0 }}
                animate={{ opacity: 1, transition: { duration: 1.5 } }}
            >
                <div className="helper">
                    <PlayIcon width={15} />
                </div>
                <BtnText
                    initial={{ opacity: 0 }}
                    animate={{ opacity: 1, transition: { duration: 1.5 } }}
                >
                    play trailer
                </BtnText>
            </AnimatedButtonContainer>
        </TrailerButtonWrapper>
    );
};

export default TrailerButton;

const TrailerButtonWrapper = styled(motion.button)`
    height: 3rem;
    width: 3rem;
    display: block;
    overflow: visible;
    background-color: #fff;
    position: relative;

    border-radius: 30px;
    border: none;
    cursor: pointer;

    &:hover {
        p {
            color: black;
        }
    }
`;

const AnimatedButtonContainer = styled(motion.div)`
    position: absolute;
    top: 0;
    left: 0;

    align-items: center;
    border: 1px solid white;
    background-color: white;
    width: 3rem;
    border-radius: 30px;
    overflow: visible;
    display: flex;
    height: 3rem;
    align-items: center;

    .helper {
        margin-left: 0.8rem;
    }

    svg {
        width: 15px;
        display: block;
    }
`;

const BtnText = styled(motion.p)`
    width: fit-content;
    display: inline-block;
    min-width: 5rem;
    margin: auto 2rem;
    min-width: 10rem;
    color: white;

    &:hover {
        color: black;
    }
`;
