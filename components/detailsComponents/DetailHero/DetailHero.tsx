import React, { FC, useState } from "react";
import { IGenre } from "../../../API_CALLS/movies.types";
import styled from "@emotion/styled";

import { motion, AnimateSharedLayout } from "framer-motion";
import { getBackgroundWallpaperPath } from "../../../utils/imageUtils";

import Genres from "./Genres";
import { fadeInSpringVariants } from "../../../animations/fadeInSpring";

import DateDisplay from "./DateDisplay";
import ProgressBar from "./ProgressBar";
import TrailerButton from "./TrailerButton";
import VideoModal from "../../VideoComponents/VideoModal";
import DetailPosterParallax from "./DetailPosterParallax";

interface DetailHeroProps {
  backdrop_path: string | null;
  poster_path: string | null;
  genres: IGenre[];
  title?: string;
  name?: string;
  videoKey?: string;
  first_air_date?: string;
  release_date?: string;
  vote_average: number;
  popularity: number;
  trailerKey: string | null;
  isCenteredPoster?: boolean;
  linkHandler?: () => void;
  linkToDetails?: boolean;
}

const DetailHero: FC<DetailHeroProps> = (props) => {
  const [showTrailer, setShowTrailer] = useState<boolean>(false);

  const handleButtonClick = () => {
    setShowTrailer((state) => !state);
  };

  return (
    <Wrapper
      backdropPath={
        props.backdrop_path
          ? getBackgroundWallpaperPath(props.backdrop_path)
          : "images/detailBcg.jpg"
      }
    >
      <FlexContainer>
        <StyledSection>
          <MainHeading
            variants={fadeInSpringVariants}
            initial="initial"
            animate="animate"
          >
            {props.title || props.name}
          </MainHeading>

          {props.genres && <Genres genres={props.genres} />}

          <DateDisplay
            heading={
              props.release_date ? "release date" : "first episode aired"
            }
            dateToDisplay={props.release_date || props.first_air_date}
          />

          <ProgressBar
            title="vote average"
            progressNumber={props.vote_average}
          />
          <ProgressBar
            title="popularity"
            progressNumber={props.popularity}
            isPopularity
          />

          {props.trailerKey && (
            <AnimateSharedLayout>
              <ButtonWrapper>
                {showTrailer ? (
                  <VideoModal
                    videoKey={props.trailerKey}
                    handleClose={handleButtonClick}
                  />
                ) : (
                  <TrailerButton onClick={handleButtonClick} />
                )}
              </ButtonWrapper>
            </AnimateSharedLayout>
          )}
        </StyledSection>
        <ParallaxWrapper>
          <DetailPosterParallax
            title={props.title || props.name}
            posterPath={props.poster_path}
            isCentered={props.isCenteredPoster}
          />
        </ParallaxWrapper>
      </FlexContainer>

      {props.linkToDetails && (
        <LinkDetailButton
          whileHover={{ scale: 1.15, borderRadius: 20 }}
          onClick={() => props.linkHandler?.()}
        >
          View Detail
        </LinkDetailButton>
      )}
    </Wrapper>
  );
};

export default DetailHero;

interface WrapperProps {
  backdropPath: string;
}

const Wrapper = styled.header<WrapperProps>`
display: flex;
flex-direction: column;
padding: 2rem 5rem;
background-repeat: no-repeat;
background-size: cover;
position: relative;

    background-image: linear-gradient(63deg, rgba(0,0,0,1) 12%, rgba(0,22,251,0.6) 85%), 
    url('${({ backdropPath }) => backdropPath}') ;
    min-height: 50vh;


    @media (max-width: 454px) {
        padding: 1rem;
    }
`;

const MainHeading = styled(motion.h1)`
  width: fit-content;
  padding-bottom: 1rem;
  border-bottom: 2px solid white;
  margin-top: 10rem;
`;

const FlexContainer = styled.div`
  display: flex;
  align-items: baseline;
  position: relative;
`;

const StyledSection = styled.section`
  flex-basis: 80%;

  @media (max-width: 460px) {
    flex-basis: 100%;
  }
`;

const ButtonWrapper = styled.div`
  margin-top: 10rem;
`;

const ParallaxWrapper = styled.div`
  align-self: flex-end;
`;

const LinkDetailButton = styled(motion.button)`
  border: none;
  background: linear-gradient(
    0deg,
    ${({ theme }) =>
      theme.colors.primaryBlue + "," + theme.colors.secondaryBlue}
  );
  color: white;
  font-weight: bold;
  font-size: 1.5rem;
  max-width: 20rem;
  margin: 1rem auto;
  padding: 1rem 2rem;
  cursor: pointer;
`;
