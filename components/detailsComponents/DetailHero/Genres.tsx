import React, { FC } from "react";
import styled from "@emotion/styled";
import { motion } from "framer-motion";
import { IGenre } from "../../../API_CALLS/movies.types";

interface GenresProps {
  genres: IGenre[];
}

const Genres: FC<GenresProps> = ({ genres }) => {
  return (
    <GenresContainer>
      <h6>genre</h6>
      <div>
        {genres.map((genre, index) => (
          <motion.span
            initial={{ opacity: 0, y: -20 }}
            animate={{
              opacity: 1,
              y: 0,
              transition: {
                delay: 0.5 + 0.15 * index,
                type: "spring",
                stiffness: 120,
                damping: 8,
              },
            }}
            key={genre.id}
            className="genre-span"
          >
            {genre.name}
          </motion.span>
        ))}
      </div>
    </GenresContainer>
  );
};

export default Genres;

const GenresContainer = styled.div`
  display: flex;
  max-width: 50rem;
  flex-wrap: wrap;
  align-items: center;

  h6 {
    margin: 0 1.5rem 0 0;
    font-size: 1.8rem;
    font-family: ${({ theme }) => theme.fonts.Oxygen};
    text-transform: capitalize;

    @media (max-width: 460px) {
      margin-bottom: 2rem;
      font-size: 1.5rem;
    }
  }

  .genre-span {
    display: inline-block;
    background-color: ${({ theme }) => theme.colors.secondaryBlueTransparent};
    padding: 0.2rem 1.5rem;
    border-radius: 4px/9px;
    border: 2px solid transparent;
    margin: 0.5rem 1rem;
    font-size: 1.4rem;
  }
`;
