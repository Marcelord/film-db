import React, { FC } from "react";
import {
  useTransform,
  useSpring,
  useViewportScroll,
  motion,
} from "framer-motion";
import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { getPosterImagePath } from "../../../utils/imageUtils";

interface DetailPosterParallaxProps {
  posterPath: string;
  isCentered?: boolean;
  title?: string;
}
const DetailPosterParallax: FC<DetailPosterParallaxProps> = ({
  posterPath,
  isCentered = false,
  title,
}) => {
  const { scrollY } = useViewportScroll();
  const cardParallax = useSpring(
    useTransform(scrollY, [0, 150], ["-18%", "25%"]),
    { stiffness: 120, damping: 9 }
  );

  return (
    <CardWrapper
      isCentered={isCentered}
      style={{ y: cardParallax }}
      initial={{ opacity: 0, y: "25%" }}
      animate={{ opacity: 1, y: 0, transition: { delay: 1.8 } }}
    >
      <img
        src={
          posterPath
            ? getPosterImagePath(posterPath)
            : "/images/placeholder.png"
        }
        alt={title || "poster"}
      />
    </CardWrapper>
  );
};

export default DetailPosterParallax;

interface CardWrapperProps {
  isCentered: boolean;
}
const CardWrapper = styled(motion.div)<CardWrapperProps>`
  width: 25rem;
  height: 40rem;
  background-color: black;
  border-radius: 12px;
  overflow: hidden;

  @media (max-width: 950px) {
    display: none !important;
  }

  margin-top: auto;
  ${({ isCentered }) =>
    !isCentered &&
    css`
      position: absolute;
      bottom: -15rem;
    `}

  img {
    height: 100%;
    margin: 0 auto;
  }
`;
