import React, { FC, useCallback } from "react";
import styled from "@emotion/styled";
import { motion } from "framer-motion";
import { useTheme } from "@emotion/react";
import {
  voteAverageProgressVariants,
  popularProgressVariants,
} from "../../../animations/progerssAniamtions";

interface ProgressBarProps {
  progressNumber: number;
  title: string;
  isPopularity?: boolean;
}

const ProgressBar: FC<ProgressBarProps> = ({
  title,
  progressNumber,
  isPopularity = false,
}) => {
  const theme = useTheme();

  const displayNumber: string | number = isPopularity
    ? progressNumber
    : progressNumber * 10 + "%";

  const getSpecificVariant = useCallback(() => {
    if (isPopularity) {
      return popularProgressVariants(
        theme.colors.primaryBlueDark,
        theme.colors.secondaryTeal,
        progressNumber
      );
    }

    return voteAverageProgressVariants(
      theme.colors.primaryBlueDark,
      theme.colors.secondaryTeal,
      progressNumber
    );
  }, [isPopularity, progressNumber]);

  return (
    <BarsContainer>
      <CaptionContainer>
        <h4>{title}</h4>
        <span>{displayNumber}</span>
      </CaptionContainer>
      <ProgressBarContainer>
        <Progress
          variants={getSpecificVariant()}
          animate="animate"
          initial="initial"
        />
      </ProgressBarContainer>
    </BarsContainer>
  );
};

export default ProgressBar;

const ProgressBarContainer = styled.div`
  min-width: 12rem;
  width: 100%;
  border-radius: 50px;
  border: 2px solid teal;
  height: 1rem;
`;

const Progress = styled(motion.div)`
  width: 100%;
  height: 100%;
  background-color: red;
`;

const BarsContainer = styled.div`
  width: 100%;
  max-width: 40rem;
  text-transform: capitalize;
`;

const CaptionContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
