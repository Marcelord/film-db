import React, { FC } from "react";
import styled from "@emotion/styled";
import { CreditsResponse } from "../../../API_CALLS/movies.types";
import { getLocalizedPrice } from "../../../utils/getLocalizedPrice";

interface DetailOverviewProps {
  overview: string;
  credits: CreditsResponse;
  budget: number;
  revenue: number;
}
const DetailOverview: FC<DetailOverviewProps> = ({
  overview,
  credits,
  budget,
  revenue,
}) => {
  const { crew, cast } = credits;
  return (
    <Wrapper>
      <Grid>
        <StyledHeading>Overview</StyledHeading>
        <MainText>{overview}</MainText>
        <InfoContainer>
          {!!budget && (
            <ValuesContainer>
              <h4>Budget</h4>
              {getLocalizedPrice(budget)}
            </ValuesContainer>
          )}
          {!!revenue && (
            <ValuesContainer>
              <h4>Revenue</h4>
              {getLocalizedPrice(revenue)}
            </ValuesContainer>
          )}
        </InfoContainer>
        {crew.length > 0 && (
          <>
            <CrewHeading>crew</CrewHeading>
            <CrewContainer>
              {crew.map(
                (member, index) =>
                  index < 5 && (
                    <PersonInfo key={member.id}>
                      <h5>{member.job}</h5>
                      <span>{member.name}</span>
                    </PersonInfo>
                  )
              )}
            </CrewContainer>
          </>
        )}
        {cast.length > 0 && (
          <>
            <CastHeading>cast</CastHeading>
            <CastContainer>
              {cast.map(
                (member, index) =>
                  index < 5 && (
                    <PersonInfo key={member.id}>
                      <h5>{member.character}</h5>
                      <span>{member.name}</span>
                    </PersonInfo>
                  )
              )}
            </CastContainer>
          </>
        )}
      </Grid>
    </Wrapper>
  );
};

export default DetailOverview;

const Wrapper = styled.div`
  background: black;
  h4 {
    font-family: ${({ theme }) => theme.fonts.Montserrat};
    margin-bottom: 1rem;
  }
`;

const Grid = styled.div`
  max-width: 70%;
  padding: 1rem 2rem;
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  grid-template-rows: repeat(6, auto);
  @media (max-width: ${({ theme }) =>
      theme.breakPoints.detailOverview.medium}) {
    max-width: unset;
    rid-template-rows: repeat(7, auto);

    grid-template-areas:
      "a a a ."
      "d d d d"
      "b b b b"
      "cH cH cH cH"
      "c c c c"
      "gH gH gH gH"
      "g g g g";
  }

  grid-template-areas:
    "a a a ."
    "b b b b"
    "cH cH . ."
    "c c c d"
    "gH gH . ."
    "g g g .";
`;

const StyledHeading = styled.h2`
  grid-area: a;
`;

const MainText = styled.p`
  grid-area: b;
`;

const InfoContainer = styled.div`
  grid-area: d;
  @media (max-width: ${({ theme }) =>
      theme.breakPoints.detailOverview.medium}) {
    display: flex;
    justify-content: space-around;
    width: 100%;
  }
`;

const ValuesContainer = styled.div`
  @media (max-width: ${({ theme }) =>
      theme.breakPoints.detailOverview.medium}) {
    text-align: center;
  }

  text-align: right;
`;

const CrewHeading = styled.h3`
  grid-area: cH;
  text-transform: uppercase;
  font-family: ${({ theme }) => theme.fonts.Oxygen};
  font-weight: 400;
  border-bottom: 1px solid white;
`;
const CastHeading = styled.h3`
  grid-area: gH;
  text-transform: uppercase;
  font-family: ${({ theme }) => theme.fonts.Oxygen};
  font-weight: 400;
  border-bottom: 1px solid white;
`;

const CrewContainer = styled.div`
  grid-area: c;
  display: flex;
  flex-flow: row wrap;
  text-align: left;
  margin: 1rem 0;
`;
const CastContainer = styled.div`
  grid-area: g;
  display: flex;
  flex-flow: row wrap;
`;

const PersonInfo = styled.div`
  margin-right: 2.5rem;
  margin-top: 1.5rem;

  h5 {
    margin: 1rem 0;
    font-size: 1.4rem;
  }
`;
