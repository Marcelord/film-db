import React, { FC, useState, useEffect } from 'react';
import styled from '@emotion/styled';
import PlayIcon from '../icons/Play';
import { getYouTubeThumbNail } from '../../utils/getYoutubeThumbNail';
import { motion, AnimateSharedLayout } from 'framer-motion';
import YouTube from 'react-youtube';
import VideoModal from '../VideoComponents/VideoModal';

interface TrailerCardProps {
    setSelectedKey: React.Dispatch<React.SetStateAction<string>>;
    videoKey: string;
    disableSmallCard: boolean;
    setDisableSmallCard: React.Dispatch<React.SetStateAction<boolean>>;
}

/**
 *
 * @param videoKey -part of YouTube url after v= query param
 * @param setSelectedKey -setState function handler
 * @description this is non reusable card with animated androidLike transition
 * to modal bare in mind that modal in due to that created with react portal but with position fixed
 */
const TrailerCard: FC<TrailerCardProps> = ({
    videoKey,
    setSelectedKey,
    disableSmallCard,
    setDisableSmallCard,
}) => {
    const handleMouseOver = () => {
        setSelectedKey(videoKey);
    };

    const [expand, setExpand] = useState<boolean>(false);

    useEffect(() => {
        if (expand) {
            setDisableSmallCard(true);

            return;
        }

        setDisableSmallCard(false);
    }, [expand]);

    const handleShrink = (
        event: React.MouseEvent<HTMLDivElement, MouseEvent>
    ) => {
        event.stopPropagation();
        setExpand(false);
    };

    return (
        <div>
            <AnimateSharedLayout type="crossfade">
                {expand ? (
                    <VideoModal
                        videoKey={videoKey}
                        handleClose={handleShrink}
                    />
                ) : (
                    <TrailerCardContainer
                        onClick={
                            !disableSmallCard
                                ? () => setExpand(true)
                                : undefined
                        }
                        whileHover={{
                            scale: 0.8,
                            transition: {
                                type: 'spring',
                                stiffness: 140,
                                damping: 5,
                            },
                        }}
                        layoutId="native-expand"
                        backRoundUrl={getYouTubeThumbNail(videoKey)}
                        onMouseEnter={handleMouseOver}
                    >
                        <PlayIcon width={30} />
                    </TrailerCardContainer>
                )}
            </AnimateSharedLayout>
        </div>
    );
};

export default TrailerCard;

interface TrailerCardContainerProps {
    backRoundUrl: string;
}

const TrailerCardContainer = styled(motion.div)<TrailerCardContainerProps>`
  border-radius: 12px;
  width: 23.9rem;
  height: 12.8rem;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 3px solid transparent;
  background: black;
  margin: 1rem;
  background: url('${({ backRoundUrl }) => backRoundUrl}');
  background-size: 140%;
  background-position: center;
  cursor: pointer;

  svg {
    path {
      fill: white;
    }
  }

`;

// const ModalContainer = styled(motion.div)`
//     position: fixed;
//     width: 80%;
//     height: 80%;
//     top: 10%;
//     left: 10%;
//     background: #000;
//     z-index: 25;
// `;
// const CloseVideoBar = styled.div`
//     position: absolute;
//     cursor: pointer;
//     font-family: ${({ theme }) => theme.fonts.Oxygen};
//     text-transform: uppercase;
//     top: 0;
//     left: 0;
//     z-index: 50;
//     display: flex;
//     align-items: center;

//     height: 3rem;
//     width: 100%;
//     border-bottom: 2px solid ${({ theme }) => theme.colors.secondaryTeal};
// `;
// const CloseText = styled.h2`
//     margin: 0 auto;
//     width: fit-content;
//     color: transparent;
//     text-align: center;
//     background-image: linear-gradient(
//         90deg,
//         ${({ theme }) => theme.colors.secondaryBlue},
//         ${({ theme }) => theme.colors.secondaryTeal}
//     );
//     background-clip: text;
//     -webkit-background-clip: text;
//     -moz-background-clip: text;
// `;

// const VideoContainer = styled.div`
//     position: absolute;
//     top: 3rem;
//     left: 0;
//     width: 100%;
//     height: calc(100% - 3rem);
//     > div {
//         height: 100%;
//     }

//     .video-iframe {
//         width: 100%;
//         height: 100%;
//     }
// `;
