import React, { FC, useCallback, useState } from 'react';
import styled from '@emotion/styled';
import RatingTriangle from '../icons/RatingTriangle';
import { motion, AnimatePresence } from 'framer-motion';

interface CollectionCardProps {
    title: string;
    rating: number;
    imagePath: string | null;
    onSelect?: () => void;
    firstAirDate?: string;
    releaseDate?: string;
}

const CollectionCard: FC<CollectionCardProps> = ({
    firstAirDate,
    releaseDate,
    title,
    rating,
    imagePath,
    onSelect,
}) => {
    const handleSelect = useCallback(() => {
        onSelect?.();
    }, [onSelect]);

    const [isHovered, setIsHovered] = useState<boolean>(false);

    return (
        <Card
            imagePath={imagePath}
            onTap={() => setIsHovered(true)}
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
            whileHover={{
                scale: 0.92,
            }}
        >
            <CardCaptionContainer>
                <CardCaption
                    animate={{
                        height: isHovered ? '28rem' : 'auto',
                        transition: {
                            type: 'spring',
                            damping: 11,
                            stiffness: 130,
                        },
                    }}
                >
                    <h6>{title}</h6>
                    <DateContainer>
                        <span>
                            {firstAirDate ? 'first episode' : 'release date'}
                        </span>
                        <span>{firstAirDate || releaseDate}</span>
                    </DateContainer>

                    <AnimatePresence>
                        {isHovered && (
                            <ViewButton
                                onClick={handleSelect}
                                initial={{ opacity: 0 }}
                                animate={{
                                    opacity: 1,
                                }}
                                exit={{ opacity: 0, y: -100 }}
                            >
                                viewDetail
                            </ViewButton>
                        )}
                    </AnimatePresence>
                </CardCaption>

                <RatingContainer animate={{ opacity: isHovered ? 0 : 1 }}>
                    <RatingTriangle rating={rating * 10} />
                </RatingContainer>
            </CardCaptionContainer>
        </Card>
    );
};

export default CollectionCard;

interface CardProps {
    imagePath: string | null;
}

const ViewButton = styled(motion.button)`
    background: white;
    color: black;
    padding: 0.5rem 1.5rem;
    border-radius: 20px;
    align-self: center;
    justify-self: center;
    border: 2px transparent solid;
    cursor: pointer;
    margin-top: auto;
    margin-bottom: auto;
    position: absolute;
    top: 50%;
`;

const Card = styled(motion.figure)<CardProps>`
    width: 19.9rem;
    height: 28.3rem;
    margin: 0 auto;
    background: ${({ imagePath }) => imagePath && `url("${imagePath}")`}
        center/cover;
    display: flex;
    flex-direction: column;
    cursor: pointer;
    justify-content: flex-end;
`;
const CardCaptionContainer = styled.div`
    justify-self: flex-end;
    position: relative;
`;

const CardCaption = styled(motion.figcaption)`
    display: flex;
    align-items: center;
    position: relative;

    flex-direction: column;
    z-index: 5;
    min-height: 3rem;
    background: ${({ theme }) => theme.colors.secondaryBlue};

    h6 {
        padding: 0.4rem;
        margin: 1rem auto 0 1rem;
        font-size: 1.4rem;
        text-shadow: 0px 3px 6px rgba(0, 0, 0);
    }
`;

const DateContainer = styled.div`
    display: flex;
    justify-content: space-between;
    margin: 1rem;
    font-size: 1.2rem;

    width: 90%;
`;

const RatingContainer = styled(motion.div)`
    position: absolute;
    right: -1.3rem;
    top: -5.2rem;
    z-index: 10;

    svg {
        height: 7.1rem;
    }
`;
