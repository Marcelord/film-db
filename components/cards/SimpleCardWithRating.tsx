import React, { FC, useCallback } from "react"
import styled from "@emotion/styled"
import RatingTriangle from "../icons/RatingTriangle"

interface SimpleCardWithRatingProps {
  title: string
  rating: number
  imagePath: string | null
  onSelect?: () => void
}

const SimpleCardWithRating: FC<SimpleCardWithRatingProps> = ({
  title,
  rating,
  imagePath,
  onSelect,
}) => {
  const handleSelect = useCallback(() => {
    onSelect?.()
  }, [onSelect])

  return (
    <Card imagePath={imagePath} onClick={handleSelect}>
      <CardCaptionContainer>
        <GlassEffect />
        <CardCaption>
          <h6>{title}</h6>
        </CardCaption>

        <RatingContainer>
          <RatingTriangle rating={rating * 10} />
        </RatingContainer>
      </CardCaptionContainer>
    </Card>
  )
}

export default SimpleCardWithRating

interface CardProps {
  imagePath: string | null
}

const Card = styled.figure<CardProps>`
  width: 15.9rem;
  height: 19.3rem;
  margin: 0 auto;
  background: ${({ imagePath }) => imagePath && `url("${imagePath}")`}
    center/cover;
  display: flex;
  flex-direction: column;
  cursor: pointer;
`
const CardCaptionContainer = styled.div`
  margin-top: auto;
  position: relative;
`

const GlassEffect = styled.div`
  background: ${({ theme }) => theme.colors.secondaryBlueTransparent};
  position: absolute;
  z-index: 1;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  backdrop-filter: blur(10px);
`

const CardCaption = styled.figcaption`
  display: flex;
  align-items: center;
  position: relative;
  height: fit-content;
  z-index: 5;
  min-height: 3rem;
  h6 {
    padding: 0.4rem;
    margin: 0;
    font-size: 1.2rem;
    text-shadow: 0px 3px 6px rgba(0, 0, 0);
  }
`
const RatingContainer = styled.div`
  position: absolute;
  right: -1.3rem;
  top: -5.2rem;
  z-index: 10;

  svg {
    height: 7.1rem;
  }
`
