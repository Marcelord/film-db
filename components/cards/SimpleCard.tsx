import React, { FC, useState } from 'react';
import styled from '@emotion/styled';
import { getPosterImagePath } from '../../utils/imageUtils';
import { title } from 'process';
import { motion, AnimatePresence } from 'framer-motion';
import { fadeOnMountVariants } from '../../animations/fadeOnMount';

interface SimpleCardProps {
    posterPath: string;
    onClick: () => void;
    title: string;
}

const SimpleCard: FC<SimpleCardProps> = ({ title, posterPath, onClick }) => {
    const [isHovered, setIsHovered] = useState<boolean>(false);
    return (
        <Container
            onTap={() => setIsHovered(true)}
            onMouseEnter={() => setIsHovered(true)}
            onMouseLeave={() => setIsHovered(false)}
            whileHover={{
                boxShadow: '8px 7px 20px 4px rgba(1, 228, 186, 0.39)',
                y: 7,
                x: -16,
                scale: 1.22,
            }}
            backgroundUrl={
                posterPath
                    ? getPosterImagePath(posterPath)
                    : '/images/placeholder.png'
            }
        >
            <TitleWrapper
                initial={{ height: '15%' }}
                animate={{ height: isHovered ? '40%' : '15%' }}
            >
                <h6>{title}</h6>
                <AnimatePresence>
                    {isHovered && (
                        <ViewButton
                            onClick={onClick}
                            variants={fadeOnMountVariants}
                            animate="animate"
                            initial="initial"
                            exit="exit"
                        >
                            See Detail
                        </ViewButton>
                    )}
                </AnimatePresence>
            </TitleWrapper>
        </Container>
    );
};

export default SimpleCard;

interface ContainerProps {
    backgroundUrl: string;
}

const Container = styled(motion.div)<ContainerProps>`
    width: 20rem;
    height: 31rem;
    background: url('${({ backgroundUrl }) => backgroundUrl}');
    background-size: cover;
    display: flex;
    flex-direction: column;
    margin: 2rem;
`;

const TitleWrapper = styled(motion.div)`
    margin-top: auto;
    background-color: ${({ theme }) => theme.colors.secondaryBlue};
    min-height: 3rem;
    display: flex;
    flex-direction: column;

    h6 {
        padding: 1rem;
        margin: 0;
        text-align: center;
        text-shadow: 0 2px 6px rgba(0, 0, 0, 0.2);
    }
`;

const ViewButton = styled(motion.button)`
    background: white;
    color: black;
    padding: 0.5rem 1.5rem;
    border-radius: 20px;
    align-self: center;
    justify-self: center;
    border: 2px transparent solid;
    cursor: pointer;
`;
