import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';

import SelectButtons from '../SelectButtons/SelectButtons';
import CardCarousel from '../carousel/CardCarousel';
import SimpleCardWithRating from '../cards/SimpleCardWithRating';

import { IReduxStore } from '../../redux/store';
import { getPosterImagePath } from '../../utils/imageUtils';
import {
    MediumType,
    getTrendingTimeWindowType,
} from '../../API_CALLS/movies.types';
import { fetchTrendingList } from '../../redux/trenindNow/trendingNow.actions';
import styled from '@emotion/styled';

const TrendingNow = () => {
    const dispatch = useDispatch();
    const router = useRouter();

    const trendingList = useSelector(
        (state: IReduxStore) => state.trendingNow.trendingList
    );

    const [timePeriod, setTimePeriod] = useState<getTrendingTimeWindowType>(
        'day'
    );

    useEffect(() => {
        dispatch(fetchTrendingList(timePeriod));
    }, [timePeriod]);

    const handleSelect = (id: number, entity: MediumType) => {
        router.push({ pathname: `/${entity}/${id}` });
    };

    return (
        <div>
            <StyledHeading>What's trending</StyledHeading>
            <SelectButtons
                firstOptionTitle={'today'}
                secondOptionTitle={'This Week'}
                firstOptionSelect={() => {
                    setTimePeriod('day');
                }}
                secondOptionSelect={() => {
                    setTimePeriod('week');
                }}
            />
            <CarouselContainer>
                <CarouselHelper>
                    <CardCarousel arrowsPosition="center">
                        {trendingList?.map((trendingItem) => {
                            return (
                                <SimpleCardWithRating
                                    key={trendingItem.id}
                                    imagePath={getPosterImagePath(
                                        trendingItem.poster_path
                                    )}
                                    rating={trendingItem.vote_average}
                                    title={
                                        trendingItem.title ||
                                        trendingItem.original_name ||
                                        ''
                                    }
                                    onSelect={() => {
                                        handleSelect(
                                            trendingItem.id,
                                            trendingItem.media_type
                                        );
                                    }}
                                />
                            );
                        })}
                    </CardCarousel>
                </CarouselHelper>
                <EqualizerHelper />
            </CarouselContainer>
        </div>
    );
};

export default TrendingNow;

const StyledHeading = styled.h4`
    padding: 0.5rem 1rem;
    margin-top: 4rem;
    margin-bottom: 0;
    border-right: 1px solid white;
    display: inline-block;
    font-family: Montserrat;
`;

const CarouselContainer = styled.div`
    position: relative;
`;

const CarouselHelper = styled.div`
    position: relative;
    z-index: 4;
`;

const EqualizerHelper = styled.div`
    position: absolute;
    bottom: 7%;
    height: 75%;
    left: 5%;
    width: 90%;
    background-image: url('images/equalizerImg.png');
    background-repeat: repeat;
    z-index: 1;
    opacity: 0.6;
`;
