import React, { FC } from 'react';
import styled from '@emotion/styled';
import Link from 'next/link';
import { motion, Variants } from 'framer-motion';
import { getTitleFromUrl } from '../../utils/getTitleFromUrl';

interface GridMenuProps {
    addressLinks: string[];
    backgroundUrl?: string;
}

const GridContainerVariants: Variants = {
    initial: {
        opacity: 0,
    },
    animate: {
        opacity: 1,
        transition: { staggerChildren: 0.12, delay: 0.3 },
    },
};

const GridItemVariants: Variants = {
    initial: {
        opacity: 0,
        x: '30%',
        y: '60%',
    },
    animate: {
        opacity: 1,
        x: 0,
        y: 0,
        transition: { type: 'tween', duration: 0.3, ease: 'easeOut' },
    },
};

const GridMenu: FC<GridMenuProps> = ({ addressLinks, backgroundUrl }) => {
    return (
        <GridContainer
            variants={GridContainerVariants}
            initial="initial"
            animate="animate"
        >
            {addressLinks.map((link, key) => (
                <GridItem
                    variants={GridItemVariants}
                    backgroundUrl={
                        backgroundUrl ? backgroundUrl : '/images/batman.jpg'
                    }
                    key={link}
                    className={`grid-item-${key + 1}`}
                    whileHover={{ scale: 0.9 }}
                >
                    <Link href={link}>
                        <a>{getTitleFromUrl(link)}</a>
                    </Link>
                </GridItem>
            ))}
        </GridContainer>
    );
};

export default GridMenu;

interface GridItemProps {
    backgroundUrl: string;
}

const GridContainer = styled(motion.div)`
    display: grid;
    margin: 0 auto;
    position: relative;
    width: 100%;
    display: grid;
    grid-template-columns: repeat(1fr 1fr 1fr 1fr 1fr);
    grid-template-rows: auto;
    grid-gap: 2rem;
    max-width: 80rem;

    grid-template-areas:
        'grid-item-1 grid-item-1 grid-item-2 grid-item-2 grid-item-2'
        'grid-item-3 grid-item-3 grid-item-3 grid-item-4 grid-item-4';

    .grid-item-1 {
        grid-area: grid-item-1;
    }

    .grid-item-2 {
        grid-area: grid-item-2;
    }

    .grid-item-3 {
        grid-area: grid-item-3;
    }

    .grid-item-4 {
        grid-area: grid-item-4;
    }
`;

const GridItem = styled(motion.div)<GridItemProps>`
  background-color: rgba(188, 183, 202, 0.6);
  height: 18rem;
  background-image: linear-gradient(
      0deg,
      rgba(6, 3, 15, 0.6),
      rgba(0, 0, 0, 0.877)
    ),
    url('${(props) => props.backgroundUrl}');
  background-attachment: fixed;
  background-size: cover;
  background-position: center;
  transition: background 0.2s ease-in;

  &:hover {
   
   background: linear-gradient(
      0deg,
      rgba(6, 3, 15, 0.8),
      rgba(0, 0, 0, 0.977)
    ),
    url('${(props) => props.backgroundUrl}');

    background-attachment: fixed;
  }


  a {
    display: flex;
    align-items: center;
    justify-content: center;
    text-decoration: none;
    color: white;
    height: 100%;
    font-size: 2rem;
    text-transform: capitalize;
    font-family: 'Oxygen';
    font-weight: 700;
  }

  @media (min-width: 500px) {
    background-size: 200%;
  }

  @media (min-width: 700px) {
    background-size: 150%;
    height: 25rem;
  }

  @media (min-width: 920px) {
    background-size: 95%;
  }
`;
