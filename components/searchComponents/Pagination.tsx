import React, { FC, useCallback } from "react";
import styled from "@emotion/styled";
import { motion } from "framer-motion";

interface PaginationProps {
  totalPages: number;
  activePage: number;
  handleClick: (arg: number) => void;
}

const Pagination: FC<PaginationProps> = ({
  totalPages,
  activePage = 1,
  handleClick,
}) => {
  return (
    <PaginationWrapper>
      {Array(totalPages)
        .fill("")
        .map((_, index) => (
          <PageNumber
            key={index}
            onClick={() => handleClick(index + 1)}
            initial={{ borderColor: "#fff", color: "#fff" }}
            animate={{
              borderColor: index + 1 === activePage ? "#01b1e1" : "#fff",
              color: index + 1 === activePage ? "#01b1e1" : "#fff",
              transition: { duration: 0.8 },
              scale: index + 1 === activePage ? 1.1 : 1,
            }}
          >
            {index + 1}{" "}
          </PageNumber>
        ))}
    </PaginationWrapper>
  );
};

export default Pagination;

const PaginationWrapper = styled.div`
  width: 250px;
  display: flex;
  justify-content: space-evenly;
  margin: 2rem auto;
`;

const PageNumber = styled(motion.button)`
  width: 3rem;
  height: 3rem;
  border: 2px solid white;
  display: flex;
  justify-content: center;
  align-items: center;
  background: none;
  border-radius: 50%;
  color: white;
  cursor: pointer;
`;
