import React, { FC } from "react";
import styled from "@emotion/styled";
import { motion } from "framer-motion";

const SearchGrid: FC = ({ children }) => {
  return (
    <Gridcontainer
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
    >
      {children}
    </Gridcontainer>
  );
};

export default SearchGrid;

const Gridcontainer = styled(motion.div)`
  display: grid;
  max-width: 1050px;
  width: 90%;
  margin: 0 auto;
  grid-template-columns: 1fr 1fr;
  grid-gap: 20px;

  @media (max-width: 800px) {
    grid-template-columns: 1fr;
  }
`;
