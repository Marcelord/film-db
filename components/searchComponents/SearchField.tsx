import React, { FC, useState, useEffect, useRef } from "react";
import styled from "@emotion/styled";

import { MediumType } from "../../API_CALLS/movies.types";
import useDetectFocus from "../../hooks/useDetectFocus";
import { motion, AnimatePresence } from "framer-motion";
import { useTheme, css } from "@emotion/react";
import EyeIcon from "../icons/EyeIcon";
import SelectButtons from "../SelectButtons/SelectButtons";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchTvSearch,
  fetchMovieSearch,
  setSearchMedium,
} from "../../redux/searching/searching.actions";
import { IReduxStore } from "../../redux/store";

interface SearchFieldProps {
  dirtyCheck?: (boolean) => void;
}

const SearchField: FC<SearchFieldProps> = ({ dirtyCheck }) => {
  const [searchKey, setSearchKey] = useState<string>("");
  const [isTyping, setIsTyping] = useState(false);

  const [displayError, setDisplayError] = useState(false);

  const {
    searchedMovies,
    searchedTv,
    isLoading,
    fetchError,
    searchedMedium,
  } = useSelector((state: IReduxStore) => state.search);

  const { isActive, ref, setActive } = useDetectFocus();
  const theme = useTheme();
  const dispatch = useDispatch();

  const isDirty = !!searchKey || isActive;

  const handleErrorDisplay = () => {
    setDisplayError(false);

    if ((searchKey && !isTyping && !isLoading) || fetchError) {
      if (searchedMedium === "tv" && searchedTv.length === 0) {
        setDisplayError(true);
      }
      if (searchedMedium === "movie" && searchedMovies.length === 0) {
        setDisplayError(true);
      }
    }
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchKey(e.target.value);
    setIsTyping(true);
  };

  useEffect(() => {
    if (searchKey) {
      searchedMedium === "tv"
        ? dispatch(fetchTvSearch(searchKey))
        : dispatch(fetchMovieSearch(searchKey));
    }
  }, [searchedMedium]);

  useEffect(() => {
    const delayedTyping = setTimeout(() => {
      setIsTyping(false);

      if (searchKey) {
        searchedMedium === "tv"
          ? dispatch(fetchTvSearch(searchKey))
          : dispatch(fetchMovieSearch(searchKey));
        dirtyCheck?.(true);
      }

      if (searchKey === "") {
        dirtyCheck?.(false);
      }
    }, 600);

    return () => clearTimeout(delayedTyping);
  }, [searchKey]);

  useEffect(() => {
    handleErrorDisplay();
  }, [isLoading, isTyping, searchedMovies, searchedMedium, searchedTv]);

  const handleReset = () => {
    setSearchKey("");
    setDisplayError(false);
  };

  return (
    <Container>
      <CustomSearch ref={ref} onClick={() => setActive(true)}>
        <SearchFieldInput
          isActive={isDirty}
          type="text"
          name="search"
          id="searchField"
          value={searchKey}
          onChange={handleChange}
        />
        <SearchLabel
          animate={{
            x: isDirty ? -35 : 0,
            y: isDirty ? -35 : 0,
            scale: isDirty ? 0.8 : 1,
            color: isDirty ? theme.colors.secondaryBlue : "#fff",
            transition: {
              type: "spring",
              stiffness: 110,
              damping: 11,
            },
          }}
          htmlFor="search"
        >
          Search
        </SearchLabel>
        <AnimatePresence exitBeforeEnter>
          {isTyping && (
            <IconContainer
              initial={{ opacity: 0, x: 20 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0 }}
            >
              <EyeIcon width={20} />
            </IconContainer>
          )}
        </AnimatePresence>
        <AnimatePresence>
          {!isTyping && searchKey && (
            <IconContainer
              initial={{ opacity: 0, x: 20 }}
              animate={{ opacity: 1, x: 0 }}
              exit={{ opacity: 0 }}
            >
              <ResetButton onClick={handleReset}>x</ResetButton>
            </IconContainer>
          )}
        </AnimatePresence>
      </CustomSearch>
      <SelectButtons
        isLightThemed
        firstOptionTitle="tv"
        secondOptionTitle="movie"
        firstOptionSelect={() => {
          dispatch(setSearchMedium("tv"));
        }}
        secondOptionSelect={() => {
          dispatch(setSearchMedium("movie"));
        }}
      />

      {displayError && (
        <ErrorMessage>Sorry we couldn't find any result</ErrorMessage>
      )}
    </Container>
  );
};

export default SearchField;

const Container = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin-top: 10rem;
`;

const IconContainer = styled(motion.div)`
  position: absolute;
  right: 0;
  bottom: 5px;
`;

const CustomSearch = styled.div`
  position: relative;
  padding: 15px 0 0;
  margin-top: 10px;
  width: 50%;
`;

const SearchLabel = styled(motion.label)`
  position: absolute;
  bottom: 0;
  display: block;
  transition: 0.2s;
  font-size: 2.2rem;
  color: whitesmoke;
`;

const SearchFieldInput = styled.input<{ isActive: boolean }>`
  &:required,
  &:invalid {
    box-shadow: none;
  }

  font-family: inherit;
  width: 100%;
  border: 0;
  border-bottom: 2px solid whitesmoke;
  outline: 0;
  font-size: 1.6rem;
  color: white;
  padding: 7px 0;
  background: transparent;
  transition: border-color 0.2s;

  border-image: linear-gradient(
    to right,
    ${({ theme, isActive }) =>
      isActive && theme.colors.secondaryBlueTransparent},
    ${({ theme, isActive }) => isActive && theme.colors.secondaryTeal}
  );
  border-image-slice: 1;

  &:focus {
    font-weight: 700;
    border-width: 3px;
  }
`;

const ErrorMessage = styled.h3`
  font-family: ${({ theme }) => theme.fonts.Oxygen};
  font-size: 1.9rem;
  text-align: center;
`;

const ResetButton = styled.button`
  border: none;
  background: transparent;
  font-family: ${({ theme }) => theme.fonts.Oxygen};
  color: white;
  font-size: 1.5rem;
  cursor: pointer;
`;
