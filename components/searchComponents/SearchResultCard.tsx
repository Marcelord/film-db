import React, { FC } from "react";
import styled from "@emotion/styled";
import { motion } from "framer-motion";
import { useSelector } from "react-redux";
import { IReduxStore } from "../../redux/store";
import { useRouter } from "next/router";

interface SearchResultCardProps {
  title: string;
  overview: string;
  placeHolderImgURL: string;
  id: number;
}

const SearchResultCard: FC<SearchResultCardProps> = ({
  title,
  overview,
  placeHolderImgURL,
  id,
}) => {
  const searchedMedium = useSelector(
    (state: IReduxStore) => state.search.searchedMedium
  );
  const router = useRouter();
  return (
    <CardContainer
      whileHover={{ scale: 1.08 }}
      onClick={() => {
        router.push(`/${searchedMedium}/${id}`);
      }}
      initial={{ opacity: 0, x: -20 }}
      animate={{
        opacity: 1,
        x: 0,
        transition: { duration: 0.6, ease: "easeOut" },
      }}
      exit={{ opacity: 0, x: 20 }}
    >
      <img src={placeHolderImgURL} alt={title} />
      <figcaption>
        <h2>{title}</h2>
        <p>{overview}</p>
      </figcaption>
    </CardContainer>
  );
};

export default SearchResultCard;

const CardContainer = styled(motion.figure)`
  margin: 0 auto;
  width: 100%;
  margin: 0;
  display: flex;
  cursor: pointer;

  height: 18rem;
  border: 2px solid ${({ theme }) => theme.colors.secondaryBlueTransparent};

  img {
    height: 100%;
    display: none;

    @media (min-width: 650px) {
      display: block;
    }
  }

  figcaption {
    display: block;
    width: 100%;
    padding: 1rem;
    display: flex;
    justify-content: flex-start;
    flex-direction: column;
    background: linear-gradient(180deg, #10274f, #01b4e4);
  }

  h2 {
    margin: 1rem 0;
    @media (max-width: 560px) {
      font-size: 2rem;
    }
  }
  p {
    display: block;
    display: flex;
    margin: auto;
    height: 9.5rem;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: pre-wrap;
  }
`;
