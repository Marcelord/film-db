import React, { FC } from "react"

interface RatingTriangleProps {
  rating: number
}

const RatingTriangle: FC<RatingTriangleProps> = ({ rating }) => {
  return (
    <svg viewBox="0 0 141 129">
      <defs>
        <filter
          id="Polygon_3"
          x="0"
          y="0"
          width="141"
          height="129"
          filterUnits="userSpaceOnUse"
        >
          <feOffset dy="6" in="SourceAlpha" />
          <feGaussianBlur stdDeviation="8" result="blur" />
          <feFlood floodOpacity="0.38" />
          <feComposite operator="in" in2="blur" />
          <feComposite in="SourceGraphic" />
        </filter>
      </defs>
      <g id="RatingTriangle" transform="translate(4720 -430.482)">
        <g
          transform="matrix(1, 0, 0, 1, -4720, 430.48)"
          filter="url(#Polygon_3)"
        >
          <path
            id="Polygon_3-2"
            data-name="Polygon 3"
            d="M46.5,0,93,81H0Z"
            transform="translate(24 18)"
            fill="#fff"
          />
        </g>
        <path
          id="Path_21"
          data-name="Path 21"
          d="M-4696,529.625l46.8-80.77,46.335,80.77Z"
          transform="translate(0 -0.144)"
          fill="none"
          stroke="#10274f"
          strokeWidth="4"
        />
        <text
          id="_98"
          data-name="98"
          transform="translate(-4662 509.482)"
          fontSize="22"
          fontFamily="SegoeUI-Semibold, Segoe UI"
          fontWeight="600"
        >
          <tspan x="0" y="0">
            {rating}
          </tspan>
        </text>
      </g>
    </svg>
  )
}

export default RatingTriangle
