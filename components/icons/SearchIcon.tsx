import React from "react"

const SearchIcon = () => {
  return (
    <svg viewBox="0 0 48.766 48.778">
      <path
        d="M52.694,49.74,39.131,36.051A19.328,19.328,0,1,0,36.2,39.022l13.474,13.6a2.087,2.087,0,0,0,2.946.076A2.1,2.1,0,0,0,52.694,49.74ZM23.943,39.187a15.262,15.262,0,1,1,10.794-4.47A15.168,15.168,0,0,1,23.943,39.187Z"
        transform="translate(-4.5 -4.493)"
        fill="#fff9f9"
      />
    </svg>
  )
}

export default SearchIcon
