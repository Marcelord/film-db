import React, { FC } from 'react';

interface EyeIconProps {
    width?: number;
    height?: number;
}
const EyeIcon: FC<EyeIconProps> = ({ width, height }) => {
    return (
        <svg width={width} height={height} viewBox="0 0 50.388 31.492">
            <defs>
                <linearGradient
                    id="a"
                    x1="0.5"
                    x2="0.5"
                    y2="1"
                    gradientUnits="objectBoundingBox"
                >
                    <stop offset="0" stopColor="#90cea1" />
                    <stop offset="1" stopColor="#01b4e4" />
                </linearGradient>
            </defs>
            <path
                d="M27.764,7.712A28.218,28.218,0,0,0,2.571,23.458a28.028,28.028,0,0,0,50.387,0A28.218,28.218,0,0,0,27.764,7.712Zm12.422,8.351a23.959,23.959,0,0,1,7.353,7.4,23.959,23.959,0,0,1-7.353,7.4,23.087,23.087,0,0,1-24.844,0,23.959,23.959,0,0,1-7.353-7.4,23.958,23.958,0,0,1,7.353-7.4c.193-.123.388-.242.584-.359a12.6,12.6,0,1,0,23.677,0q.294.175.583.359ZM27.764,18.734a4.724,4.724,0,1,1-4.724-4.724A4.724,4.724,0,0,1,27.764,18.734Z"
                transform="translate(-2.571 -7.712)"
                fill="url(#a)"
            />
        </svg>
    );
};

export default EyeIcon;
