import React from "react"

const FilmIcon = () => {
  return (
    <svg viewBox="0 0 48.333 48.333">
      <g
        id="Icon_feather-film"
        data-name="Icon feather-film"
        transform="translate(-0.5 -0.5)"
      >
        <path
          fill="none"
          stroke="#fff"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="5"
          id="Path_2"
          data-name="Path 2"
          d="M7.723,3H41.61a4.723,4.723,0,0,1,4.723,4.723V41.61a4.723,4.723,0,0,1-4.723,4.723H7.723A4.723,4.723,0,0,1,3,41.61V7.723A4.723,4.723,0,0,1,7.723,3Z"
        />
        <path
          fill="none"
          stroke="#fff"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="5"
          id="Path_3"
          data-name="Path 3"
          d="M10.5,3V46.333"
          transform="translate(3.333 0)"
        />
        <path
          fill="none"
          stroke="#fff"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="5"
          id="Path_4"
          data-name="Path 4"
          d="M25.5,3V46.333"
          transform="translate(10 0)"
        />
        <path
          fill="none"
          stroke="#fff"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="5"
          id="Path_5"
          data-name="Path 5"
          d="M3,18H46.333"
          transform="translate(0 6.667)"
        />
        <path
          fill="none"
          stroke="#fff"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="5"
          id="Path_6"
          data-name="Path 6"
          d="M3,10.5H13.833"
          transform="translate(0 3.333)"
        />
        <path
          fill="none"
          stroke="#fff"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="5"
          id="Path_7"
          data-name="Path 7"
          d="M3,25.5H13.833"
          transform="translate(0 10)"
        />
        <path
          fill="none"
          stroke="#fff"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="5"
          id="Path_8"
          data-name="Path 8"
          d="M25.5,25.5H36.333"
          transform="translate(10 10)"
        />
        <path
          fill="none"
          stroke="#fff"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="5"
          id="Path_9"
          data-name="Path 9"
          d="M25.5,10.5H36.333"
          transform="translate(10 3.333)"
        />
      </g>
    </svg>
  )
}

export default FilmIcon
