import React from "react"

const HomeIcon = () => {
  return (
    <svg viewBox="0 0 45.334 39.877">
      <path
        id="Icon_metro-home"
        fill="#fff"
        d="M47.9,26.876,25.238,9.817,2.571,26.876V19.92L25.238,2.861,47.9,19.921Zm-5.667-.62V42.739H30.9V31.75H19.571V42.739H8.237V26.256l17-12.362Z"
        transform="translate(-2.571 -2.861)"
      />
    </svg>
  )
}

export default HomeIcon
