import React, { FC } from "react"
import { motion } from "framer-motion"

interface PlayIconProps {
  height?: number
  width?: number
}

const PlayIcon: FC<PlayIconProps> = ({ width, height }) => {
  return (
    <motion.svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox="0 0 49.388 56.447"
    >
      <motion.path
        id="Icon_awesome-play"
        data-name="Icon awesome-play"
        d="M46.788,23.668,7.982.726A5.262,5.262,0,0,0,0,5.279V51.152a5.287,5.287,0,0,0,7.982,4.553L46.788,32.774a5.286,5.286,0,0,0,0-9.106Z"
        transform="translate(0 -0.002)"
        fill="#000"
      />
    </motion.svg>
  )
}

export default PlayIcon
