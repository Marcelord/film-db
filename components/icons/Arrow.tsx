import React from "react"

 const Arrow = () => {
  return (
    <svg viewBox="0 0 52 52">
      <defs>
        <linearGradient
          id="a"
          x1="0.5"
          x2="0.5"
          y2="1"
          gradientUnits="objectBoundingBox"
        >
          <stop offset="0" stopColor="#90cea1" />
          <stop offset="1" stopColor="#01b4e4" />
        </linearGradient>
      </defs>
      <path
        d="M3.375,29.375a26,26,0,1,0,26-26A26,26,0,0,0,3.375,29.375Zm30.563,0L23.7,19.237a2.413,2.413,0,0,1,3.413-3.412L39.038,27.788a2.41,2.41,0,0,1,.075,3.325L27.363,42.9a2.409,2.409,0,1,1-3.413-3.4Z"
        transform="translate(-3.375 -3.375)"
        fill="url(#a)"
      />
    </svg>
  )
}

export default Arrow
