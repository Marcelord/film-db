import React from "react"

const TvIcon = () => {
  return (
    <svg viewBox="0 0 62.813 46.192">
      <g transform="translate(-50.845 -573.84)">
        <g transform="translate(49.861 568.355)">
          <path
            d="M57.777,5.484H6.992A6.019,6.019,0,0,0,.984,11.492V39.453a6.019,6.019,0,0,0,6.007,6.007h50.8A6.019,6.019,0,0,0,63.8,39.453V11.492A6.039,6.039,0,0,0,57.777,5.484ZM60.151,39.44a2.376,2.376,0,0,1-2.374,2.374H6.992A2.376,2.376,0,0,1,4.617,39.44V11.492A2.376,2.376,0,0,1,6.992,9.117h50.8a2.376,2.376,0,0,1,2.374,2.374V39.44Z"
            transform="translate(0)"
            fill="#fff"
          />
          <path
            d="M40.4,28.547H10.817a1.817,1.817,0,0,0,0,3.633H40.4a1.817,1.817,0,1,0,0-3.633Z"
            transform="translate(6.776 19.496)"
            fill="#fff"
          />
        </g>
        <text
          transform="translate(71 601)"
          fill="#fff"
          fontSize="18"
          fontFamily="SegoeUI-Semibold, Segoe UI"
          fontWeight="600"
        >
          <tspan x="0" y="0">
            TV
          </tspan>
        </text>
      </g>
    </svg>
  )
}

export default TvIcon
