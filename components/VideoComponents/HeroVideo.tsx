import React, { FC, useState, useEffect } from "react";
import YouTube, { Options } from "react-youtube";
import styled from "@emotion/styled";

import { isMobile } from "../../utils/detectMobileBrowser";
import TrailersScrollBar from "./TrailersScrollBar";
import { fetchTrailersList } from "../../redux/homeTrailers/homeTrailers.actions";
import { IMovieResult } from "../../API_CALLS/movies.types";
import { useSelector, useDispatch } from "react-redux";
import { IReduxStore } from "../../redux/store";

interface HeroVideoProps {
  initialTrailerKeys: string[];
}

const HeroVideo: FC<HeroVideoProps> = ({ initialTrailerKeys }) => {
  const [selectedVideoUrl, setSelectedVideoUrl] = useState<string>("");
  const [shouldPlayVideo, setShouldPlayVideo] = useState(true);
  const dispatch = useDispatch();

  const movies: IMovieResult[] = useSelector(
    (state: IReduxStore) => state.hero.heroMovieList
  );
  const trailersKeys = useSelector(
    (state: IReduxStore) => state.homeTrailers.trailersIDs
  );

  // honestly could not figure out what kind of event youtube iframe fires
  const onVideoEnd = (event: any) => {
    event.target.playVideo();
  };

  useEffect(() => {
    if (isMobile()) {
      setShouldPlayVideo(false);
    }
  }, []);

  useEffect(() => {
    if (
      movies.length !== 0 &&
      trailersKeys.length === 0 &&
      initialTrailerKeys.length === 0
    ) {
      dispatch(fetchTrailersList(movies));
    }
  }, [movies, trailersKeys]);

  const videoOptions: Options = {
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      autoplay: 1,
      controls: 0,
      rel: 0,
      showinfo: 0,
      mute: 1,
      loop: 1,
    },
  };

  return (
    <>
      <div>
        <StyledHeading>Watch Trailers</StyledHeading>
      </div>
      <TrailersWrapper>
        {shouldPlayVideo && (
          <div className="video-background">
            <div className="video-foreground">
              <YouTube
                videoId={selectedVideoUrl || "1j2sXLbzm9U"}
                opts={videoOptions}
                className="video-iframe"
                onEnd={onVideoEnd}
              />
            </div>
          </div>
        )}
        {initialTrailerKeys.length === 0 && trailersKeys.length === 0 ? (
          <ErrorText>
            we are sorry, at this moment we have no trailers to display
          </ErrorText>
        ) : (
          <Content>
            <TrailersScrollBar
              setSelectedVideoKey={setSelectedVideoUrl}
              videoKeys={initialTrailerKeys}
            />
          </Content>
        )}
      </TrailersWrapper>
    </>
  );
};

export default HeroVideo;

const TrailersWrapper = styled.div`
  position: relative;
  min-height: 50vh;
  display: flex;
  align-items: center;
  background: linear-gradient(
      180deg,
      rgb(11, 27, 55) 0%,
      rgba(0, 31, 255, 0.22) 51%,
      rgb(11, 57, 138) 100%
    ),
    url("/images/detailBcg.jpg");

  .video-background {
    overflow: hidden;
    background: #000;
    position: absolute;
    top: 0;
    bottom: 0;
    right: 0;
    left: 0;
    z-index: 1;
    &::after {
      display: block;
      content: "";
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 1;
      background: linear-gradient(
        180deg,
        rgb(11, 27, 55) 0%,
        rgba(0, 31, 255, 0.22) 51%,
        rgb(11, 57, 138) 100%
      );
    }
  }

  .video-foreground,
  .video-background iframe {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    pointer-events: none;
  }

  @media (min-aspect-ratio: 16/9) {
    .video-foreground {
      height: 300%;
      top: -100%;
    }
  }

  .video-foreground {
    height: 300%;
    top: -100%;
  }
  @media (max-aspect-ratio: 16/9) {
    .video-foreground {
      width: 300%;
      left: -100%;
    }
  }
`;

const Content = styled.div`
  position: relative;
  z-index: 5;
  width: 100%;
`;

const StyledHeading = styled.h4`
  margin-left: auto;
  padding: 0.5rem 1rem;
  border-left: 1px solid white;
  width: fit-content;
  font-family: ${({ theme }) => theme.fonts.Montserrat};
`;

const ErrorText = styled.h4`
  text-align: center;
  margin: 0 auto;
`;
