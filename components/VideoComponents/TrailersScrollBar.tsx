import React, { FC, useState } from 'react';
import ScrollBar from 'react-scrollbars-custom';
import styled from '@emotion/styled';

import TrailerCard from '../cards/TrailerCard';

interface TrailersScrollBarProps {
    setSelectedVideoKey: React.Dispatch<React.SetStateAction<string>>;
    videoKeys: string[];
}

const TrailersScrollBar: FC<TrailersScrollBarProps> = ({
    setSelectedVideoKey,
    videoKeys,
}) => {
    const [disableSmallCard, setDisableSmallCard] = useState(false);

    return (
        <TrailersRailWrapper>
            <ScrollBar
                trackXProps={{ className: 'scrollTrack' }}
                thumbXProps={{ className: 'scrollThumb' }}
            >
                <TrailersRail>
                    {videoKeys &&
                        videoKeys.map((videoKey) => (
                            <TrailerCard
                                key={videoKey}
                                setSelectedKey={setSelectedVideoKey}
                                videoKey={videoKey}
                                disableSmallCard={disableSmallCard}
                                setDisableSmallCard={setDisableSmallCard}
                            />
                        ))}
                </TrailersRail>
            </ScrollBar>
        </TrailersRailWrapper>
    );
};

export default TrailersScrollBar;

const TrailersRailWrapper = styled.div`
    /* overflow-x: scroll; */
    width: 85%;
    margin: 0 auto;
    height: 18rem;

    .scrollTrack {
        background: transparent;
        border: 1px solid
            ${({ theme }) => theme.colors.secondaryBlueTransparent};
        padding: 3px;
        height: 14px !important;
    }

    .scrollThumb {
        background: teal !important;
        background: linear-gradient(
            45deg,
            ${({ theme }) => theme.colors.secondaryBlue},
            ${({ theme }) => theme.colors.secondaryTeal}
        );
    }
`;

const TrailersRail = styled.div`
    width: fit-content;
    display: flex;
`;
