import React, { FC } from 'react';
import styled from '@emotion/styled';
import { motion } from 'framer-motion';
import YouTube from 'react-youtube';

interface VideoModalProps {
    handleClose: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    videoKey: string;
}

const VideoModal: FC<VideoModalProps> = ({ handleClose, videoKey }) => {
    return (
        <ModalContainer layoutId="native-expand">
            <CloseVideoBar onClick={handleClose}>
                <CloseText>Close X</CloseText>
            </CloseVideoBar>
            <VideoContainer>
                <YouTube videoId={videoKey} className="video-iframe" />
            </VideoContainer>
        </ModalContainer>
    );
};

export default VideoModal;

const ModalContainer = styled(motion.div)`
    position: fixed;
    width: 80%;
    height: 80%;
    top: 10%;
    left: 10%;
    background: #000;
    z-index: 25;
`;
const CloseVideoBar = styled.div`
    position: absolute;
    cursor: pointer;
    font-family: ${({ theme }) => theme.fonts.Oxygen};
    text-transform: uppercase;
    top: 0;
    left: 0;
    z-index: 50;
    display: flex;
    align-items: center;

    height: 3rem;
    width: 100%;
    border-bottom: 2px solid ${({ theme }) => theme.colors.secondaryTeal};
`;
const CloseText = styled.h2`
    margin: 0 auto;
    width: fit-content;
    color: transparent;
    text-align: center;
    background-image: linear-gradient(
        90deg,
        ${({ theme }) => theme.colors.secondaryBlue},
        ${({ theme }) => theme.colors.secondaryTeal}
    );
    background-clip: text;
    -webkit-background-clip: text;
    -moz-background-clip: text;
`;

const VideoContainer = styled.div`
    position: absolute;
    top: 3rem;
    left: 0;
    width: 100%;
    height: calc(100% - 3rem);
    > div {
        height: 100%;
    }

    .video-iframe {
        width: 100%;
        height: 100%;
    }
`;
