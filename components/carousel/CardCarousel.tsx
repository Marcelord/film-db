import React, { FC } from "react";

import Slider, { Settings } from "react-slick";

import styled from "@emotion/styled";

interface CardCarouselProps {
  arrowsPosition: "bottom" | "center";
}

const CardCarousel: FC<CardCarouselProps> = ({ arrowsPosition, children }) => {
  const settings: Settings = {
    dots: true,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 950,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 649,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          initialSlide: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };

  return (
    <CarouselWrapper position={arrowsPosition}>
      <Slider {...settings}>{children}</Slider>
    </CarouselWrapper>
  );
};

export default CardCarousel;

interface wrapperInterface {
  position: "bottom" | "center";
}

const CarouselWrapper = styled.div<wrapperInterface>`
  max-width: 80rem;
  height: 25rem;
  position: relative;
  margin: 0 auto;
  width: 85%;

  .react-multi-carousel-list {
    overflow: hidden;
  }
  .react-multi-carousel-track {
    padding-left: 0;
  }

  ul {
    display: inline-flex;
    overflow: hidden;

    li {
      display: block;
    }
  }

  .slick-prev,
  .slick-next {
    ${({ position }) => position === "bottom" && "top: 90%;"}
    width: 3.5rem;
    height: 3.5rem;
  }

  .slick-prev:before,
  .slick-next:before {
    color: ${({ theme }) => theme.colors.secondaryTeal};
    font-size: 3.5rem;
  }

  .slick-dots {
    display: flex !important;
    justify-content: center;

    @media (max-width: ${({ theme }) => theme.breakPoints.carousel.small}) {
      display: none !important;
    }
  }

  .slick-dots li button:before {
    color: ${({ theme }) => theme.colors.secondaryTeal};
  }
`;
