import React, { FC } from "react";
import styled from "@emotion/styled";
import Link from "next/link";

import HomeIcon from "../components/icons/HomeIcon";
import { baseRoutes } from "../constants/Routes";
import FilmIcon from "../components/icons/FilmIcon";
import TvIcon from "../components/icons/TvIcon";
import SearchIcon from "../components/icons/SearchIcon";
import { motion } from "framer-motion";

interface SideMenuProps {
  isActive: boolean;
}

const SideMenu: FC<SideMenuProps> = ({ isActive }) => {
  return (
    <SideMenuStyled
      initial={{ x: 0 }}
      animate={{
        x: isActive ? 0 : "-100%",
        transition: { duration: 0.18, type: "tween", ease: "easeOut" },
      }}
    >
      <Link href={baseRoutes.home}>
        <a>
          <HomeIcon />
          <RouteCaption>Home</RouteCaption>
        </a>
      </Link>
      <Link href={baseRoutes.movies}>
        <a>
          <FilmIcon />
          <RouteCaption>Movies</RouteCaption>
        </a>
      </Link>
      <Link href={baseRoutes.tvShows}>
        <a>
          <TvIcon />
          <RouteCaption>tv Shows</RouteCaption>
        </a>
      </Link>
      <Link href={baseRoutes.search}>
        <a>
          <SearchIcon />
          <RouteCaption>Search</RouteCaption>
        </a>
      </Link>
    </SideMenuStyled>
  );
};

export default SideMenu;

const SideMenuStyled = styled(motion.aside)`
  color: white;
  width: 100%;

  background: black;
  position: fixed;
  width: 6rem;
  top: 4rem;
  left: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  z-index: 50;

  a {
    text-align: center;
    margin-bottom: 2.2rem;
    display: block;
    max-width: 3.5rem;

    &:first-child {
      margin-top: 3rem;
    }

    svg {
      width: 25px;
    }
  }
`;

const RouteCaption = styled.span`
  display: block;
  text-align: center;
  font-size: 1.1rem;
`;
