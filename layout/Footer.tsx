import React from "react";
import styled from "@emotion/styled";

const Footer = () => {
  return (
    <StyledFooter>
      <Notation>
        <h6>this site was created on top of MovieDB</h6>
        <a href="https://www.themoviedb.org/">check them out here</a>
      </Notation>
      <AuthorHeading>
        Marcel Konjata © {new Date().getFullYear()}, Built with Next js
      </AuthorHeading>
      <Image src="/images/movie_logo_footer.svg" alt="movie db" />
    </StyledFooter>
  );
};

export default Footer;

const StyledFooter = styled.div`
  background: black;
  color: white;
  display: flex;
  padding: 2rem 6rem;
  justify-content: space-between;

  @media (max-width: 600px) {
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;

const AuthorHeading = styled.h6`
  margin: 0;
  align-self: flex-end;
  @media (max-width: 600px) {
    align-self: center;
    margin: 2rem 0;
    order: 3;
  }
`;

const Image = styled.img`
  width: 6rem;
`;

const Notation = styled.div`
  max-width: 15rem;
  @media (max-width: 600px) {
    max-width: 25rem;
    text-align: center;
    margin-bottom: 2rem;
  }

  h6 {
    font-size: 1.4rem;
    margin: 0;
  }
  a {
    font-size: 1.2rem;
  }
`;
