import React, { FC } from "react";
import Link from "next/link";
import styled from "@emotion/styled";

import Logo from "../components/icons/Logo";

interface HeaderProps {
  isToggled: boolean;
  toggleNav: () => void;
}
const Header: FC<HeaderProps> = ({ toggleNav, isToggled }) => (
  <StyledHeader>
    <Hamburger onClick={toggleNav}>
      <svg
        className={`ham hamRotate ham7 ${isToggled ? "active" : ""} `}
        viewBox="0 0 100 100"
        width="80"
      >
        <path
          className="line top"
          d="m 70,33 h -40 c 0,0 -6,1.368796 -6,8.5 0,7.131204 6,8.5013 6,8.5013 l 20,-0.0013"
        />
        <path className="line middle" d="m 70,50 h -40" />
        <path
          className="line bottom"
          d="m 69.575405,67.073826 h -40 c -5.592752,0 -6.873604,-9.348582 1.371031,-9.348582 8.244634,0 19.053564,21.797129 19.053564,12.274756 l 0,-40"
        />
      </svg>
    </Hamburger>

    <LogoContainer>
      <Link href="/">
        <a>
          <Logo />
        </a>
      </Link>
    </LogoContainer>
  </StyledHeader>
);

export default Header;

const StyledHeader = styled.header`
  background-color: black;
  height: 4rem;
  min-height: 4rem;
  display: flex;
  align-items: center;
  justify-content: center;
  position: fixed;
  z-index: 50;
  width: 100%;
`;
const LogoContainer = styled.div`
  width: 16rem;
  margin-right: auto;
  margin-left: auto;
`;

const Hamburger = styled.div`
  cursor: pointer;
  background: none;
  border: none;

  .ham {
    transition: transform 400ms;
    height: 4rem;
    width: 4rem;
    margin-left: 1.5rem;
  }
  .hamRotate.active {
    transform: rotate(45deg);
  }
  .hamRotate180.active {
    transform: rotate(180deg);
  }
  .line {
    fill: none;
    stroke-width: 2px;
    transition: stroke-dasharray 400ms, stroke-dashoffset 400ms;
    stroke: #fff;
    stroke-width: 5.5;
    stroke-linecap: round;
  }
  .ham7 .top {
    stroke-dasharray: 40 82;
  }
  .ham7 .middle {
    stroke-dasharray: 40 111;
  }
  .ham7 .bottom {
    stroke-dasharray: 40 161;
  }
  .ham7.active .top {
    stroke-dasharray: 17 82;
    stroke-dashoffset: -62px;
  }
  .ham7.active .middle {
    stroke-dashoffset: 23px;
  }
  .ham7.active .bottom {
    stroke-dashoffset: -83px;
  }
`;
