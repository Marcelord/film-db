import React, { FC, useState, useEffect } from "react";
import { AnimatePresence, motion } from "framer-motion";

import Header from "./Header";
import Footer from "./Footer";
import SideMenu from "./Sidemenu";

import styled from "@emotion/styled";
import { isMobile } from "../utils/detectMobileBrowser";

const Layout: FC = ({ children }) => {
  const [isNavOpen, setNavOpen] = useState<boolean>(true);

  //   this checks on initial load if userAgent is mobile device and toggle menu
  // thanks to initial animation of page there will be no flash at all
  useEffect(() => {
    if (isMobile()) {
      setNavOpen(false);
    }
  }, []);

  const toggleNav = () => {
    //asynchronous set call to save little performance
    setNavOpen((state) => !state);
  };

  return (
    <StyledWrapper>
      <Header isToggled={isNavOpen} toggleNav={toggleNav} />
      <Container>
        <SideMenuContainer
          initial={{ width: "6.8rem" }}
          animate={{
            width: isNavOpen ? "6.8rem" : 0,
            transition: { duration: 0.18, type: "tween", ease: "easeOut" },
          }}
        >
          <SideMenu isActive={isNavOpen} />
        </SideMenuContainer>
        <StyledMain>{children}</StyledMain>
      </Container>
      <Footer />
    </StyledWrapper>
  );
};

export default Layout;

const StyledMain = styled.main`
  overflow: hidden;
  width: 100%;
  margin-top: 4rem;
  flex-grow: 1;
  background: linear-gradient(black 15%, #10274f 70%, #005dff);
`;
const StyledWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const Container = styled.div`
  flex-shrink: 0;
  flex-grow: 1;
  flex-basis: auto;
  display: flex;
`;

const SideMenuContainer = styled(motion.div)`
  position: relative;
  width: 6.8rem;
  height: 100%;
  background: black;
`;
