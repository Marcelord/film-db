import { useEffect, useState } from 'react';

export function useWindowScrollCheck() {
    const [showScrollToTop, setShowScrollToTop] = useState(false);

    useEffect(() => {
        const handleScroll = () => {
            if (window.scrollY > 400 && showScrollToTop === false) {
                setShowScrollToTop(true);
            }

            if (window.scrollY <= 400) {
                showScrollToTop === true && setShowScrollToTop(false);
            }
        };

        window.addEventListener('scroll', handleScroll);

        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, [showScrollToTop]);

    return { showScrollToTop };
}
