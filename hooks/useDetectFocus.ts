import { useRef, useState, useEffect } from 'react';

const useDetectFocus = () => {
    const [isActive, setActive] = useState(false);
    const ref = useRef(null);

    const handleClick = (event: MouseEvent) => {
        if (ref.current && !ref.current.contains(event.target)) {
            setActive(false);
        }
    };

    useEffect(() => {
        document.addEventListener('click', handleClick, true);
        return () => {
            document.removeEventListener('click', handleClick, true);
        };
    });

    return { ref, isActive, setActive };
};

export default useDetectFocus;
