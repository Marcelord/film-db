export interface MoviesResponse {
    page: number;
    total_results: number;
    total_pages: number;
    results: IMovieResult[];
}
export interface TVResponse {
    page: number;
    total_results: number;
    total_pages: number;
    results: Array<ITVResult>;
}

export interface IMovieResult {
    poster_path: string | null;
    popularity: number;
    id: number;
    adult?: boolean;
    backdrop_path: string | null;
    vote_average: number;
    overview: string;
    release_date: string;
    genre_ids: Array<number>;
    original_title: string;
    original_language: string;
    title: string;
    vote_count: number;
    video: boolean;
}

export interface ITVResult {
    poster_path: string | null;
    popularity: number;
    id: number;
    backdrop_path: string | null;
    vote_average: number;
    overview: string;
    first_air_date: string;
    origin_country: string[];
    genre_ids: number[];
    original_language: string;
    vote_count: number;
    name: string;
    original_name: string;
    total_results: number;
    total_pages: number;
}

export interface IRecommendedResult {
    first_air_date?: string;
    origin_country?: string[];
    name?: string;
    original_name?: string;
    total_results?: number;
    total_pages?: number;
    poster_path: string | null;
    popularity: number;
    id: number;
    adult?: boolean;
    backdrop_path: string | null;
    vote_average: number;
    overview: string;
    release_date?: string;
    genre_ids: Array<number>;
    original_title?: string;
    original_language: string;
    title?: string;
    vote_count: number;
    video?: boolean;
}

export type MediumType = 'movie' | 'tv';

export interface TrailersResponse {
    id: number;

    results: ITrailers[];
}

export interface ITrailers {
    id: string;
    iso_639_1: string;
    iso_3166_1: string;
    key: string;
    name: string;
    site: string;
    size: number;
    type: string;
}

export type getTrendingTimeWindowType = 'day' | 'week';

export enum getTrendingTimeWindow {
    TODAY = 'day',
    THIS_WEEK = 'week',
}

export interface TrendingResponse {
    page: number;
    results: ITrendingResult[];
    total_pages: number;
    total_results: number;
}

export interface ITrendingResult {
    poster_path: string | null;
    adult: boolean;
    overview: string;
    release_date: string;
    genre_ids: number[];
    id: number;
    original_title?: string;
    original_name?: string;
    original_language: string;
    title: string;
    backdrop_path: string | null;
    popularity: number;
    vote_count: number;
    video: boolean;
    vote_average: number;
    media_type: MediumType;
}

export interface ICreatedBy {
    id: number;
    credit_id: string;
    name: string;
    gender: number;
    profile_path: string;
}

export interface IGenre {
    name: string;
    id: number;
}

export interface TvByIdResponse {
    backdrop_path: string;
    createdBy: ICreatedBy[];
    episode_run_time: number[];
    first_air_date: string;
    genres: IGenre[];
    homepage: string;
    id: number;
    in_production: boolean;
    languages: string[];
    last_air_date: string;
    number_of_episodes: number;
    number_of_seasons: number;
    origin_country: string[];
    original_language: string;
    original_name: string;
    overview: string;
    popularity: number;
    poster_path: string | null;
    vote_average: number;
    vote_count: number;
    name: string;
}

export interface IProductionCompany {
    name: string;
    id: number;
    logo_path: string | null;
    origin_country: string;
}

export interface IProductionCountry {
    iso_3166_1: string;
    name: string;
}

export interface ISpokenLangs {
    iso_639_1: string;
    name: string;
}

export interface MovieByIdResponse {
    adult: boolean;
    backdrop_path: string | null;
    belongs_to_collection: null | object;
    budget: number;
    genres: IGenre[];
    homepage: string | null;
    id: number;
    imdb_id: string | null;
    original_language: string;
    original_title: string;
    overview: string | null;
    popularity: number;
    poster_path: string | null;
    production_companies: IProductionCompany[];
    production_countries: IProductionCountry[];
    release_date: string;
    revenue: number;
    runtime: number | null;
    spoken_languages: ISpokenLangs[];
    status: string;
    tagline: string | null;
    title: string;
    video: boolean;
    vote_average: number;
    vote_count: number;
}

export interface ICrew {
    credit_id: string;
    department: string;
    gender: number | null;
    id: number;
    job: string;
    name: string;
    profile_path: string | null;
}

export interface ICast {
    cast_id?: number;
    character: string;
    credit_id: string;
    gender: number | null;
    id: number;
    name: string;
    order: number;
    profile_path: string | null;
}

export interface CreditsResponse {
    cast: ICast[];
    crew: ICrew[];
    id: number;
}

export interface RecommendedResponse {
    page: number;
    total_pages: number;
    total_result: number;
    results: IRecommendedResult[];
}

export enum MoviesSlugs {
    POPULAR = 'popular',
    TOP_RATED = 'top-rated',
    IN_CINEMA = 'in-cinema',
    UPCOMING = 'upcoming',
}

export enum MoviesCollections {
    POPULAR = 'popular',
    TOP_RATED = 'top_rated',
    NOW_PLAYING = 'now_playing',
    UPCOMING = 'upcoming',
}

export enum TVsSlugs {
    POPULAR = 'popular',
    TOP_RATED = 'top-rated',
    AIR_TODAY = 'airing-today',
    LATEST = 'latest',
}

export enum TvsCollections {
    POPULAR = 'popular',
    TOP_RATED = 'top_rated',
    AIR_TODAY = 'airing_today',
    LATEST = 'on_the_air',
}
