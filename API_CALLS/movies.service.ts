import Axios from 'axios';
import {
    MoviesResponse,
    TVResponse,
    MediumType,
    TrailersResponse,
    TrendingResponse,
    getTrendingTimeWindowType,
    TvByIdResponse,
    MovieByIdResponse,
    CreditsResponse,
    RecommendedResponse,
    MoviesSlugs,
    MoviesCollections,
    TVsSlugs,
    TvsCollections,
} from './movies.types';

// read token is safe to have exposed, so i will not do process env
const READ_TOKEN =
    'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3YWIxNDczNDRhYjJiY2VkMjU3NDcxMDM0ZDJiM2ZhOSIsInN1YiI6IjVlZTkzODE5NmUzYjY4MDAzMmIzM2JmOCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.rLy2TIWaT7Dg8Dkpt5PifsvTJzIXsTtXuZ9lybDazeY';

export const MovieService = Axios.create({
    baseURL: 'https://api.themoviedb.org/3/',
    headers: { Authorization: `Bearer ${READ_TOKEN}` },
});

export const getPopularMovies = () => {
    return MovieService.get<MoviesResponse>('movie/popular');
};

export const getPopularTV = () => {
    return MovieService.get<TVResponse>('tv/popular');
};

export const getTrailers = (mediumType: MediumType, id: string | number) => {
    return MovieService.get<TrailersResponse>(`${mediumType}/${id}/videos`);
};

export const getTrending = (timeWindow: getTrendingTimeWindowType) => {
    return MovieService.get<TrendingResponse>(`trending/all/${timeWindow}`);
};

export const getMovieById = (id: number) => {
    return MovieService.get<MovieByIdResponse>(`movie/${id}`);
};

export const getTVId = (id: number) => {
    return MovieService.get<TvByIdResponse>(`tv/${id}`);
};

export const getCredits = (mediumType: MediumType, id: number) => {
    return MovieService.get<CreditsResponse>(`${mediumType}/${id}/credits`);
};

export const getRecommended = (mediumType: MediumType, id: number) => {
    return MovieService.get<RecommendedResponse>(
        `${mediumType}/${id}/recommendations`
    );
};

export const getSearchResults = (mediumType: MediumType, keyword: string) => {
    return MovieService.get<MoviesResponse | TVResponse>(
        `search/${mediumType}`,
        {
            params: {
                query: keyword,
                // well this is on your own danger to set true
                include_adult: false,
            },
        }
    );
};

export const slugMovieCollections = (slug: MoviesSlugs, page?: number) => {
    let collection: MoviesCollections;
    switch (slug) {
        case MoviesSlugs.UPCOMING:
            collection = MoviesCollections.UPCOMING;
            break;
        case MoviesSlugs.TOP_RATED:
            collection = MoviesCollections.TOP_RATED;
            break;
        case MoviesSlugs.IN_CINEMA:
            collection = MoviesCollections.NOW_PLAYING;
            break;
        case MoviesSlugs.POPULAR:
            collection = MoviesCollections.POPULAR;
            break;

        default:
            break;
    }

    return MovieService.get<MoviesResponse>(`movie/${collection}`, {
        params: { page },
    });
};

export const slugTVCollections = (slug: TVsSlugs, page?: number) => {
    let collection: TvsCollections;
    switch (slug) {
        case TVsSlugs.LATEST:
            collection = TvsCollections.LATEST;
            break;
        case TVsSlugs.TOP_RATED:
            collection = TvsCollections.TOP_RATED;
            break;
        case TVsSlugs.AIR_TODAY:
            collection = TvsCollections.AIR_TODAY;
            break;
        case TVsSlugs.POPULAR:
            collection = TvsCollections.POPULAR;
            break;

        default:
            break;
    }

    return MovieService.get<TVResponse>(`tv/${collection}`, {
        params: { page },
    });
};
