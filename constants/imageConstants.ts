export const baseImageUrl = "http://image.tmdb.org/t/p/"
export const secureImageUrl = "https://image.tmdb.org/t/p/"
export const backdropSizes = ["w300", "w780", "w1280", "original"]
export const logoSizes = [
  "w45",
  "w92",
  "w154",
  "w185",
  "w300",
  "w500",
  "original",
]
export const posterSizes = [
  "w92",
  "w154",
  "w185",
  "w342",
  "w500",
  "w780",
  "original",
]
export const profileSizes = ["w45", "w185", "h632", "original"]
export const stillSizes = ["w92", "w185", "w300", "original"]
