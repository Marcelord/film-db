export const baseRoutes = {
    home: '/',
    movies: '/movies',
    tvShows: '/tv-shows',
    search: '/search',
    movie: '/movie',
    tv: '/tv',
};

export const moviesRoutes = {
    popular: `${baseRoutes.movies}/popular`,
    topRated: `${baseRoutes.movies}/top-rated`,
    inCinema: `${baseRoutes.movies}/in-cinema`,
    upcoming: `${baseRoutes.movies}/upcoming`,
};
export const tvShowsRoutes = {
    popular: `${baseRoutes.tvShows}/popular`,
    topRated: `${baseRoutes.tvShows}/top-rated`,
    inCinema: `${baseRoutes.tvShows}/airing-today`,
    upcoming: `${baseRoutes.tvShows}/latest`,
};

export const AllowedMoviesSlugs = [
    'popular',
    'top-rated',
    'in-cinema',
    'upcoming',
];

export const AllowedTvsSlugs = [
    'popular',
    'top-rated',
    'airing-today',
    'latest',
];
