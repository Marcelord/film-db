import "@emotion/react";

declare module "@emotion/react" {
  export interface Theme {
    colors: {
      primaryBlueDark: string;
      primaryBlue: string;
      secondaryBlue: string;
      secondaryTeal: string;
      secondaryBlueTransparent: string;
    };

    fonts: {
      Roboto: string;
      Montserrat: string;
      Oxygen: string;
    };

    breakPoints: {
      hero: {
        small: string;
      };
      carousel: {
        small: string;
      };
      detailOverview: {
        medium: string;
      };
    };
  }
}
