export const theme = {
  colors: {
    primaryBlueDark: "#10274f",
    primaryBlue: "#005dff",
    secondaryBlue: "#01b4e4",
    secondaryTeal: "#90cea1",
    secondaryBlueTransparent: "#01b4e4a8",
  },

  fonts: {
    Roboto: "Roboto",
    Montserrat: "Montserrat",
    Oxygen: "Oxygen",
  },

  breakPoints: {
    hero: {
      small: "580px",
    },
    carousel: {
      small: "480px",
    },
    detailOverview: {
      medium: "950px",
    },
  },
};
