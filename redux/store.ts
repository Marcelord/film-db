import { createStore, combineReducers, applyMiddleware } from 'redux';
import { heroReducer, IHeroState } from './hero/hero.reducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {
    homeTrailersReducer,
    IHomeTrailersState,
} from './homeTrailers/homeTrailers.reducer';

import trendingNowReducer, {
    ITrendingNowState,
} from './trenindNow/trendingNow.reducer';

import {
    ICollectionsState,
    collectionsReducer,
} from './collections/collections.reducer';

import { ISearchState, searchReducer } from './searching/searching.reducer';

const middleWare = [thunk];

export interface IReduxStore {
    hero: IHeroState;
    homeTrailers: IHomeTrailersState;
    trendingNow: ITrendingNowState;
    collections: ICollectionsState;
    search: ISearchState;
}

const rootReducer = combineReducers({
    hero: heroReducer,
    homeTrailers: homeTrailersReducer,
    trendingNow: trendingNowReducer,
    collections: collectionsReducer,
    search: searchReducer,
});

export const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(...middleWare))
);
