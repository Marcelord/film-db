import { Dispatch } from "redux"
import { AxiosError } from "axios"
import {
  ITrendingResult,
  getTrendingTimeWindowType,
} from "../../API_CALLS/movies.types"
import { getTrending } from "../../API_CALLS/movies.service"

export enum trendingNowActions {
  FETCH_TRENDING_LIST_START = "[Hero] fetch trending list START ",
  FETCH_TRENDING_LIST_SUCCESS = "[Hero] fetch trending list SUCCESS ",
  FETCH_TRENDING_LIST_ERROR = "[Hero] fetch trending list ERROR ",
}

interface fetchTrendingListStart {
  type: trendingNowActions.FETCH_TRENDING_LIST_START
}

interface fetchTrendingListSuccess {
  payload: ITrendingResult[]
  type: trendingNowActions.FETCH_TRENDING_LIST_SUCCESS
}

interface fetchTrendingListError {
  payload: AxiosError
  type: trendingNowActions.FETCH_TRENDING_LIST_ERROR
}

export const fetchTrendingListStart = () => {
  return { type: trendingNowActions.FETCH_TRENDING_LIST_START }
}

export const fetchTrendingListSuccess = (payload: ITrendingResult[]) => {
  return { type: trendingNowActions.FETCH_TRENDING_LIST_SUCCESS, payload }
}

export const fetchTrendingListError = (payload: AxiosError) => {
  return { type: trendingNowActions.FETCH_TRENDING_LIST_ERROR, payload }
}

// TODO: proper types form API + async await, make api service layer

export const fetchTrendingList = (
  timeWindow: getTrendingTimeWindowType
) => async (dispatch: Dispatch) => {
  try {
    dispatch(fetchTrendingListStart())

    const { data } = await getTrending(timeWindow)

    dispatch(fetchTrendingListSuccess(data.results))
  } catch (error) {
    dispatch(fetchTrendingListError(error))
  }
}

export type trendingNowActionsTypes =
  | fetchTrendingListSuccess
  | fetchTrendingListStart
  | fetchTrendingListError
