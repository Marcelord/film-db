import { ITrendingResult } from "../../API_CALLS/movies.types"
import { AxiosError } from "axios"
import {
  trendingNowActionsTypes,
  trendingNowActions,
} from "./trendingNow.actions"

export interface ITrendingNowState {
  trendingList: ITrendingResult[]
  isLoading: boolean
  fetchError: AxiosError | null
}

const initialState: ITrendingNowState = {
  trendingList: [],
  isLoading: false,
  fetchError: null,
}

export default (state = initialState, action: trendingNowActionsTypes) => {
  switch (action.type) {
    case trendingNowActions.FETCH_TRENDING_LIST_START:
      return { ...state, isLoading: true }
    case trendingNowActions.FETCH_TRENDING_LIST_ERROR:
      return { ...state, isLoading: false, fetchError: action.payload }
    case trendingNowActions.FETCH_TRENDING_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        fetchError: null,
        trendingList: action.payload,
      }
    default:
      return state
  }
}
