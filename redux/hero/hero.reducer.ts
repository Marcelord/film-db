import { IMovieResult, ITVResult } from "../../API_CALLS/movies.types"
import { AxiosError } from "axios"
import { heroActionTypes, HeroActions } from "./hero.actions"

export type activeListType = "tv" | "movie"

export interface IHeroState {
  selectedMovieHero: IMovieResult | null
  selectedTVHero: ITVResult | null
  heroMovieList: IMovieResult[]
  heroTVList: ITVResult[]
  isLoading: boolean
  dataError: AxiosError | null
  activeList: activeListType
}

const HeroState: IHeroState = {
  selectedMovieHero: null,
  selectedTVHero: null,
  heroMovieList: [],
  heroTVList: [],
  isLoading: false,
  dataError: null,
  activeList: "movie",
}

export const heroReducer = (state = HeroState, action: heroActionTypes) => {
  switch (action.type) {
    case HeroActions.FETCH_HERO_LIST_START:
      return { ...state, isLoading: true }
    case HeroActions.FETCH_HERO_MOVIE_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        heroMovieList: action.payload,
        dataError: null,
      }
    case HeroActions.FETCH_HERO_TV_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        heroTVList: action.payload,
        dataError: null,
      }
    case HeroActions.FETCH_HERO_LIST_ERROR:
      return { ...state, isLoading: false, dataError: action.payload }
    case HeroActions.SET_HERO_MOVIE_ITEM:
      return {
        ...state,
        selectedMovieHero: state.heroMovieList.find(
          item => item.id === action.payload
        ),
      }
    case HeroActions.SET_HERO_TV_ITEM:
      return {
        ...state,
        selectedTVHero: state.heroTVList.find(
          item => item.id === action.payload
        ),
      }
    case HeroActions.SET_ACTIVE_LIST_TYPE:
      return {
        ...state,
        activeList: action.payload,
      }
    default:
      return state
  }
}
