import { Dispatch } from "redux"
import { IMovieResult, ITVResult } from "../../API_CALLS/movies.types"
import { AxiosError } from "axios"
import { getPopularMovies, getPopularTV } from "../../API_CALLS/movies.service"
import { activeListType } from "./hero.reducer"

export enum HeroActions {
  SET_HERO_MOVIE_ITEM = "[hero] set movie item",
  SET_HERO_TV_ITEM = "[hero] set TV item",
  SET_ACTIVE_LIST_TYPE = "[hero] set active list type",
  FETCH_HERO_LIST_START = "[hero] fetch list start",
  FETCH_HERO_MOVIE_LIST_SUCCESS = "[hero] fetch movie list success",
  FETCH_HERO_TV_LIST_SUCCESS = "[hero] fetch tv list success",
  FETCH_HERO_LIST_ERROR = "[hero] fetch list error",
  RESET_HERO_ITEM = "[hero] reset selected",
}

interface fetchHeroListStart {
  type: HeroActions.FETCH_HERO_LIST_START
}

interface fetchHeroMovieListSuccess {
  type: HeroActions.FETCH_HERO_MOVIE_LIST_SUCCESS
  payload: IMovieResult[]
}

interface fetchHeroTVListSuccess {
  type: HeroActions.FETCH_HERO_TV_LIST_SUCCESS
  payload: ITVResult[]
}

interface fetchHeroListError {
  type: HeroActions.FETCH_HERO_LIST_ERROR
  payload: AxiosError
}

interface setHeroMovieItem {
  type: HeroActions.SET_HERO_MOVIE_ITEM
  payload: number
}

interface setHeroTVItem {
  type: HeroActions.SET_HERO_TV_ITEM
  payload: number
}

interface setActiveListType {
  type: HeroActions.SET_ACTIVE_LIST_TYPE
  payload: activeListType
}

export const setActiveListType = (activeType: activeListType) => {
  return { type: HeroActions.SET_ACTIVE_LIST_TYPE, payload: activeType }
}

export const fetchHeroListStart = () => {
  return { type: HeroActions.FETCH_HERO_LIST_START }
}

export const fetchHeroMovieListSuccess = (payload: IMovieResult[]) => {
  return { type: HeroActions.FETCH_HERO_MOVIE_LIST_SUCCESS, payload }
}

export const fetchHeroTVListSuccess = (payload: ITVResult[]) => {
  return { type: HeroActions.FETCH_HERO_TV_LIST_SUCCESS, payload }
}

export const fetchHeroListError = (error: AxiosError) => {
  return { type: HeroActions.FETCH_HERO_LIST_ERROR, payload: error }
}

export const setHeroMovieItem = (id: number | string) => {
  return { type: HeroActions.SET_HERO_MOVIE_ITEM, payload: id }
}

export const setHeroTVItem = (id: number | string) => {
  return { type: HeroActions.SET_HERO_TV_ITEM, payload: id }
}

// async actions

export const fetchHeroMovieList = () => async (dispatch: Dispatch) => {
  try {
    dispatch(fetchHeroListStart())

    const { data } = await getPopularMovies()
    dispatch(fetchHeroMovieListSuccess(data.results))
    dispatch(setActiveListType("movie"))
  } catch (error) {
    dispatch(fetchHeroListError(error))
  }
}

export const fetchHeroTVList = () => async (dispatch: Dispatch) => {
  try {
    dispatch(fetchHeroListStart())

    const { data } = await getPopularTV()
    dispatch(fetchHeroTVListSuccess(data.results))
    dispatch(setActiveListType("tv"))
  } catch (error) {
    dispatch(fetchHeroListError(error))
  }
}

export type heroActionTypes =
  | fetchHeroListStart
  | fetchHeroMovieListSuccess
  | fetchHeroTVListSuccess
  | fetchHeroListError
  | setHeroMovieItem
  | setActiveListType
  | setHeroTVItem
