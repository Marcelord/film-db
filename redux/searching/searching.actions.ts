import {
    TVResponse,
    MoviesResponse,
    ITVResult,
    IMovieResult,
    MediumType,
} from '../../API_CALLS/movies.types';
import { AxiosError } from 'axios';
import { Dispatch } from 'redux';
import { getSearchResults } from '../../API_CALLS/movies.service';

export enum searchActions {
    SET_SEARCH_MEDIUM = '[search] set medium',
    FETCH_MOVIE_SEARCH_START = '[search] FETCH_MOVIE_SEARCH_START',
    FETCH_MOVIE_SEARCH_SUCCESS = '[search] FETCH_MOVIE_SEARCH_SUCCESS',
    FETCH_MOVIE_SEARCH_ERROR = '[search] FETCH_MOVIE_SEARCH_ERROR',
    FETCH_TV_SEARCH_START = '[search] FETCH_TV_SEARCH_START',
    FETCH_TV_SEARCH_SUCCESS = '[search] FETCH_TV_SEARCH_SUCCESS',
    FETCH_TV_SEARCH_ERROR = '[search] FETCH_TV_SEARCH_ERROR',
}

interface setSearchMedium {
    type: searchActions;
    payload: MediumType;
}

interface fetchTvSearchStart {
    type: searchActions.FETCH_TV_SEARCH_START;
}

interface fetchTvSearchSuccess {
    type: searchActions.FETCH_TV_SEARCH_SUCCESS;
    payload: TVResponse;
}

interface fetchTvSearchError {
    type: searchActions.FETCH_TV_SEARCH_ERROR;
    payload: AxiosError;
}

interface fetchMovieSearchStart {
    type: searchActions.FETCH_MOVIE_SEARCH_START;
}

interface fetchMovieSearchSuccess {
    type: searchActions.FETCH_MOVIE_SEARCH_SUCCESS;
    payload: MoviesResponse;
}

interface fetchMovieSearchError {
    type: searchActions.FETCH_MOVIE_SEARCH_ERROR;
    payload: AxiosError;
}

export const setSearchMedium = (medium: MediumType) => {
    return { type: searchActions.SET_SEARCH_MEDIUM, payload: medium };
};

export const fetchTvSearchStart = () => {
    return {
        type: searchActions.FETCH_TV_SEARCH_START,
    };
};

export const fetchTvSearchSuccess = (payload: ITVResult[]) => ({
    type: searchActions.FETCH_TV_SEARCH_SUCCESS,
    payload,
});

export const fetchTvSearchError = (payload: AxiosError) => ({
    type: searchActions.FETCH_TV_SEARCH_ERROR,
    payload,
});

export const fetchMovieSearchStart = () => ({
    type: searchActions.FETCH_MOVIE_SEARCH_START,
});

export const fetchMovieSearchSuccess = (payload: IMovieResult[]) => ({
    type: searchActions.FETCH_MOVIE_SEARCH_SUCCESS,
    payload,
});

export const fetchMovieSearchError = (payload: AxiosError) => ({
    type: searchActions.FETCH_MOVIE_SEARCH_ERROR,
    payload,
});

export const fetchMovieSearch = (search: string) => async (
    dispatch: Dispatch
) => {
    try {
        dispatch(fetchMovieSearchStart());

        const { data } = await getSearchResults('movie', search);

        dispatch(fetchMovieSearchSuccess(data.results as IMovieResult[]));
    } catch (error) {
        dispatch(fetchMovieSearchError(error as AxiosError));
    }
};

export const fetchTvSearch = (search: string) => async (dispatch: Dispatch) => {
    try {
        dispatch(fetchTvSearchStart());

        const { data } = await getSearchResults('tv', search);

        dispatch(fetchTvSearchSuccess(data.results as ITVResult[]));
    } catch (error) {
        dispatch(fetchTvSearchError(error as AxiosError));
    }
};

export type searchActionsTypes =
    | fetchTvSearchStart
    | fetchTvSearchSuccess
    | fetchTvSearchError
    | fetchMovieSearchStart
    | fetchMovieSearchSuccess
    | fetchMovieSearchError
    | setSearchMedium;
