import { searchActionsTypes, searchActions } from "./searching.actions";
import {
  IMovieResult,
  ITVResult,
  MediumType,
} from "../../API_CALLS/movies.types";
import { AxiosError } from "axios";

export interface ISearchState {
  searchedMovies: IMovieResult[];
  searchedTv: ITVResult[];
  isLoading: boolean;
  fetchError: AxiosError | null;
  searchedMedium: MediumType;
}

const initialState: ISearchState = {
  searchedMovies: [],
  searchedTv: [],
  isLoading: false,
  fetchError: null,
  searchedMedium: "tv",
};

export const searchReducer = (
  state = initialState,
  action: searchActionsTypes
) => {
  switch (action.type) {
    case searchActions.FETCH_MOVIE_SEARCH_ERROR:
    case searchActions.FETCH_TV_SEARCH_ERROR:
      return { ...state, fetchError: action.payload, isLoading: false };

    case searchActions.FETCH_MOVIE_SEARCH_START:
    case searchActions.FETCH_TV_SEARCH_START:
      return { ...state, fetchError: null, isLoading: true };

    case searchActions.FETCH_MOVIE_SEARCH_SUCCESS:
      return {
        ...state,
        searchedMovies: action.payload,
        isLoading: false,
      };

    case searchActions.SET_SEARCH_MEDIUM:
      return {
        ...state,
        searchedMedium: action.payload,
      };
    case searchActions.FETCH_TV_SEARCH_SUCCESS:
      return { ...state, searchedTv: action.payload, isLoading: false };
    default:
      return state;
  }
};
