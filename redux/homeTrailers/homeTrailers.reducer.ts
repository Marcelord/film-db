import {
  homeTrailersActionTypes,
  HomeTrailersActions,
} from "./homeTrailers.actions"
import { AxiosError } from "axios"

export interface IHomeTrailersState {
  trailersIDs: string[]

  isLoading: boolean
  error: AxiosError | null
}

const initialState: IHomeTrailersState = {
  trailersIDs: [],
  isLoading: false,
  error: null,
}

export const homeTrailersReducer = (
  state = initialState,
  action: homeTrailersActionTypes
) => {
  switch (action.type) {
    case HomeTrailersActions.FETCH_TRAILER_LIST_START:
      return { ...state, isLoading: true, error: null }
    case HomeTrailersActions.FETCH_TRAILER_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        trailersIDs: action.payload,
        error: null,
      }
    case HomeTrailersActions.FETCH_TRAILER_LIST_ERROR:
      return { ...state, isLoading: false, error: action.payload }
    default:
      return state
  }
}
