import { Dispatch } from "redux"
import { IMovieResult } from "../../API_CALLS/movies.types"
import { AxiosError } from "axios"
import { getTrailers } from "../../API_CALLS/movies.service"

const Trailer = "Trailer"
const YouTube = "YouTube"

export enum HomeTrailersActions {
  FETCH_TRAILER_LIST_START = "[HOME TRAILERS] fetch start",
  FETCH_TRAILER_LIST_SUCCESS = "[HOME TRAILERS] fetch success",
  FETCH_TRAILER_LIST_ERROR = "[HOME TRAILERS] fetch success",
}

interface fetchTrailersListStart {
  type: HomeTrailersActions.FETCH_TRAILER_LIST_START
}

interface fetchTrailersListSuccess {
  type: HomeTrailersActions.FETCH_TRAILER_LIST_SUCCESS
  payload: string[]
}

interface fetchTrailersListError {
  type: HomeTrailersActions.FETCH_TRAILER_LIST_ERROR
  payload: AxiosError
}

export const fetchTrailersListStart = () => {
  return { type: HomeTrailersActions.FETCH_TRAILER_LIST_START }
}

export const fetchTrailersListSuccess = (payload: string[]) => {
  return {
    type: HomeTrailersActions.FETCH_TRAILER_LIST_SUCCESS,
    payload,
  }
}

export const fetchTrailersListError = (payload: AxiosError) => {
  return {
    type: HomeTrailersActions.FETCH_TRAILER_LIST_ERROR,
    payload,
  }
}

export const fetchTrailersList = (moviesList: IMovieResult[]) => async (
  dispatch: Dispatch
) => {
  dispatch(fetchTrailersListStart())
  try {
    const promiseArray = moviesList.map(movieItem =>
      getTrailers("movie", movieItem.id)
    )

    const response = await Promise.all(promiseArray)

    const datas = response.map(({ data }) =>
      data.results.find(
        result => result.type === Trailer && result.site === YouTube
      )
    )

    const trailersIds = datas
      .map(data => data?.key)
      .filter(key => key !== undefined)

    dispatch(fetchTrailersListSuccess(trailersIds as string[]))
  } catch (error) {
    return dispatch(fetchTrailersListError(error))
  }
}

export type homeTrailersActionTypes =
  | fetchTrailersListStart
  | fetchTrailersListSuccess
  | fetchTrailersListError
