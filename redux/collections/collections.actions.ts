import {
    TVResponse,
    MoviesResponse,
    MoviesSlugs,
    TVsSlugs,
} from '../../API_CALLS/movies.types';
import { AxiosError } from 'axios';
import { Dispatch } from 'redux';
import {
    slugMovieCollections,
    slugTVCollections,
} from '../../API_CALLS/movies.service';

export enum collectionsActions {
    FETCH_MOVIE_COLLECTION_START = '[Collections] FETCH_MOVIE_COLLECTION_START',
    FETCH_MOVIE_COLLECTION_SUCCESS = '[Collections] FETCH_MOVIE_COLLECTION_SUCCESS',
    FETCH_MOVIE_COLLECTION_ERROR = '[Collections] FETCH_MOVIE_COLLECTION_ERROR',
    FETCH_TV_COLLECTION_START = '[Collections] FETCH_TV_COLLECTION_START',
    FETCH_TV_COLLECTION_SUCCESS = '[Collections] FETCH_TV_COLLECTION_SUCCESS',
    FETCH_TV_COLLECTION_ERROR = '[Collections] FETCH_TV_COLLECTION_ERROR',
}

interface fetchTvCollectionStart {
    type: collectionsActions.FETCH_TV_COLLECTION_START;
}

interface fetchTvCollectionSuccess {
    type: collectionsActions.FETCH_TV_COLLECTION_SUCCESS;
    payload: TVResponse;
}

interface fetchTvCollectionError {
    type: collectionsActions.FETCH_TV_COLLECTION_ERROR;
    payload: AxiosError;
}

interface fetchMovieCollectionStart {
    type: collectionsActions.FETCH_MOVIE_COLLECTION_START;
}

interface fetchMovieCollectionSuccess {
    type: collectionsActions.FETCH_MOVIE_COLLECTION_SUCCESS;
    payload: MoviesResponse;
}

interface fetchMovieCollectionError {
    type: collectionsActions.FETCH_MOVIE_COLLECTION_ERROR;
    payload: AxiosError;
}

export const fetchTvCollectionStart = () => {
    return {
        type: collectionsActions.FETCH_TV_COLLECTION_START,
    };
};

export const fetchTvCollectionSuccess = (payload: TVResponse) => ({
    type: collectionsActions.FETCH_TV_COLLECTION_SUCCESS,
    payload,
});

export const fetchTvCollectionError = (payload: AxiosError) => ({
    type: collectionsActions.FETCH_TV_COLLECTION_ERROR,
    payload,
});

export const fetchMovieCollectionStart = () => ({
    type: collectionsActions.FETCH_MOVIE_COLLECTION_START,
});

export const fetchMovieCollectionSuccess = (payload: MoviesResponse) => ({
    type: collectionsActions.FETCH_MOVIE_COLLECTION_SUCCESS,
    payload,
});

export const fetchMovieCollectionError = (payload: AxiosError) => ({
    type: collectionsActions.FETCH_MOVIE_COLLECTION_ERROR,
    payload,
});

export const fetchMovieCollection = (page: number, slug: string) => async (
    dispatch: Dispatch
) => {
    try {
        dispatch(fetchMovieCollectionStart());

        const { data } = await slugMovieCollections(slug as MoviesSlugs, page);

        dispatch(fetchMovieCollectionSuccess(data));
    } catch (error) {
        dispatch(fetchMovieCollectionError(error as AxiosError));
    }
};

export const fetchTvCollection = (page: number, slug: string) => async (
    dispatch: Dispatch
) => {
    try {
        dispatch(fetchTvCollectionStart());

        const { data } = await slugTVCollections(slug as TVsSlugs, page);

        dispatch(fetchTvCollectionSuccess(data));
    } catch (error) {
        dispatch(fetchTvCollectionError(error as AxiosError));
    }
};

export type collectionsActionsTypes =
    | fetchTvCollectionStart
    | fetchTvCollectionSuccess
    | fetchTvCollectionError
    | fetchMovieCollectionStart
    | fetchMovieCollectionSuccess
    | fetchMovieCollectionError;
