import {
    collectionsActionsTypes,
    collectionsActions,
} from './collections.actions';
import { MoviesResponse, TVResponse } from '../../API_CALLS/movies.types';
import { AxiosError } from 'axios';

export interface ICollectionsState {
    moviesCollection: MoviesResponse | null;
    tvCollection: TVResponse | null;
    isLoading: boolean;
    fetchError: AxiosError | null;
}

const initialState: ICollectionsState = {
    moviesCollection: {
        page: 0,
        results: null,
        total_pages: null,
        total_results: null,
    },
    tvCollection: {
        page: 0,
        results: null,
        total_pages: null,
        total_results: null,
    },
    isLoading: false,
    fetchError: null,
};

export const collectionsReducer = (
    state = initialState,
    action: collectionsActionsTypes
) => {
    switch (action.type) {
        case collectionsActions.FETCH_MOVIE_COLLECTION_ERROR:
        case collectionsActions.FETCH_TV_COLLECTION_ERROR:
            return { ...state, fetchError: action.payload, isLoading: false };

        case collectionsActions.FETCH_MOVIE_COLLECTION_START:
        case collectionsActions.FETCH_TV_COLLECTION_START:
            return { ...state, fetchError: null, isLoading: true };

        case collectionsActions.FETCH_MOVIE_COLLECTION_SUCCESS:
            return addMovieCollection(state, action.payload);

        case collectionsActions.FETCH_TV_COLLECTION_SUCCESS:
            return addTvCollection(state, action.payload);
        default:
            return state;
    }
};

function addMovieCollection(state: ICollectionsState, payload: MoviesResponse) {
    const { moviesCollection } = state;

    if (moviesCollection.results !== null) {
        const collection =
            moviesCollection.page === payload.page
                ? payload
                : {
                      ...payload,
                      results: state.moviesCollection.results.concat(
                          payload.results
                      ),
                  };

        return {
            ...state,
            moviesCollection: collection,
            fetchError: null,
            isLoading: null,
        };
    }

    return {
        ...state,
        moviesCollection: payload,
        fetchError: null,
        isLoading: null,
    };
}

function addTvCollection(state: ICollectionsState, payload: TVResponse) {
    const { tvCollection } = state;

    if (tvCollection.results !== null) {
        const collection =
            tvCollection.page === payload.page
                ? payload
                : {
                      ...payload,
                      results: state.tvCollection.results.concat(
                          payload.results
                      ),
                  };

        return {
            ...state,
            tvCollection: collection,
            fetchError: null,
            isLoading: null,
        };
    }

    return {
        ...state,
        tvCollection: payload,
        fetchError: null,
        isLoading: null,
    };
}
