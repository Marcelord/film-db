import React from "react";
import { GetServerSideProps, GetServerSidePropsContext, NextPage } from "next";
import {
  TvByIdResponse,
  CreditsResponse,
  IRecommendedResult,
} from "../../API_CALLS/movies.types";
import { AxiosError } from "axios";
import { AnimatePresence, motion } from "framer-motion";
import { pageSlideVariant } from "../../animations/pageSlide";
import DetailHero from "../../components/detailsComponents/DetailHero/DetailHero";
import DetailOverview from "../../components/detailsComponents/DetailOverview/DetailOverview";
import {
  getTrailers,
  getTVId,
  getCredits,
  getRecommended,
} from "../../API_CALLS/movies.service";
import styled from "@emotion/styled";
import DetailRecommended from "../../components/detailsComponents/DetailRecommended/DetailRecommended";
import { PageWrapper, ErrorContainer } from "../../shared/detailByIdPagesStyle";
import OrbitSpinner from "../../components/loader/OrbitSpinner";

interface TVShowDetailPageInitialProps {
  tvDetail: TvByIdResponse;
  initialFetchError: AxiosError | null;
  trailerKey: string | null;
  credits: CreditsResponse | null;
  recommended: IRecommendedResult[] | null;
}

const TVShowDetailPage: NextPage<TVShowDetailPageInitialProps> = ({
  tvDetail,
  initialFetchError,
  trailerKey,
  credits,
  recommended,
}) => {
  return (
    <AnimatePresence initial={true} exitBeforeEnter>
      <PageWrapper
        variants={pageSlideVariant}
        initial="initial"
        animate="animate"
        exit="exit"
      >
        {initialFetchError && (
          <ErrorContainer>
            <OrbitSpinner size={160} />
            <h1>Something went wrong</h1>
            <h2>try to reload the page or different tv show</h2>
          </ErrorContainer>
        )}

        {!initialFetchError && (
          <>
            <DetailHero
              backdrop_path={tvDetail?.backdrop_path}
              name={tvDetail?.name}
              genres={tvDetail?.genres}
              first_air_date={tvDetail?.first_air_date}
              poster_path={tvDetail?.poster_path}
              popularity={tvDetail?.popularity}
              vote_average={tvDetail?.vote_average}
              trailerKey={trailerKey}
            />
            <DetailOverview
              budget={null}
              revenue={null}
              overview={tvDetail.overview}
              credits={credits}
            />
            <DetailRecommended mediumType="tv" recommendations={recommended} />
          </>
        )}
      </PageWrapper>
    </AnimatePresence>
  );
};

export default TVShowDetailPage;

export const getServerSideProps: GetServerSideProps<TVShowDetailPageInitialProps> = async (
  context: GetServerSidePropsContext
) => {
  const { id } = context.params;

  let tvDetail: TvByIdResponse | null;
  let initialFetchError: AxiosError | null;
  let trailerKey: string | null;
  let credits: CreditsResponse | null;
  let recommended: IRecommendedResult[] | null;
  const idAsNumber = Number(id);

  try {
    const { data } = await getTVId(idAsNumber);
    tvDetail = data;
    initialFetchError = null;
  } catch (error) {
    initialFetchError = error as AxiosError;
    tvDetail = null;
  }

  try {
    const { data } = await getTrailers("tv", idAsNumber);

    const trailer = data.results.find(
      (result) => result.type === "Trailer" && result.site === "YouTube"
    );

    trailerKey = trailer.key;
  } catch (trailerFetchError) {
    trailerKey = null;
  }

  try {
    const { data } = await getCredits("tv", idAsNumber);
    credits = data;
  } catch (creditsError) {
    credits = null;
  }

  try {
    const { data } = await getRecommended("tv", idAsNumber);
    recommended = data.results;
  } catch (recommendedError) {
    recommended = null;
  }
  return {
    props: {
      tvDetail,
      credits,
      trailerKey,
      initialFetchError,
      recommended,
    },
  };
};
