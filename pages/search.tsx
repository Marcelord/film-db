import React, { useState, useEffect, useCallback } from "react";
import { NextPage } from "next";
import { AnimatePresence, motion } from "framer-motion";
import styled from "@emotion/styled";
import SearchField from "../components/searchComponents/SearchField";
import { useSelector } from "react-redux";
import { IReduxStore } from "../redux/store";
import { Loading } from "../shared/slugPageStyles";
import OrbitSpinner from "../components/loader/OrbitSpinner";
import { pageSlideVariant } from "../animations/pageSlide";
import SearchResultCard from "../components/searchComponents/SearchResultCard";
import { getPosterImagePath } from "../utils/imageUtils";
import SearchGrid from "../components/searchComponents/SearchGrid";
import Pagination from "../components/searchComponents/Pagination";

const RESULTS_PER_PAGE = 6;

const search: NextPage = () => {
  const { isLoading, searchedMedium, searchedMovies, searchedTv } = useSelector(
    (state: IReduxStore) => state.search
  );

  const [pagesToDisplay, setPagesToDisplay] = useState<number>(null);
  const [activePage, setActivePage] = useState<number>(1);

  useEffect(() => {
    const mediumToCalculate =
      searchedMedium === "tv" ? searchedTv.length : searchedMovies.length;

    const pagesToDisplay = Math.ceil(mediumToCalculate / RESULTS_PER_PAGE);
    setActivePage(1);

    setPagesToDisplay(pagesToDisplay);
  }, [searchedTv, searchedMovies, searchedMedium]);

  const handlePageSet = (page: number) => {
    setActivePage(page);
  };

  const pageTVSelect = useCallback(() => {
    const startIndex = activePage * RESULTS_PER_PAGE - RESULTS_PER_PAGE;
    const endIndex = startIndex + RESULTS_PER_PAGE - 1;

    if (pagesToDisplay === activePage) {
      return searchedTv.slice(startIndex);
    }

    return searchedTv.slice(startIndex, endIndex + 1);
  }, [pagesToDisplay, activePage, searchedTv]);

  const pageMovieSelect = useCallback(() => {
    const startIndex = activePage * RESULTS_PER_PAGE - RESULTS_PER_PAGE;
    const endIndex = startIndex + RESULTS_PER_PAGE - 1;

    if (pagesToDisplay === activePage) {
      return searchedMovies.slice(startIndex);
    }

    return searchedMovies.slice(startIndex, endIndex + 1);
  }, [pagesToDisplay, activePage, searchedMovies]);

  return (
    <AnimatePresence>
      <PageWrapper
        variants={pageSlideVariant}
        initial="initial"
        animate="animate"
        exit="exit"
      >
        <SearchField />

        {isLoading && (
          <Loading>
            <OrbitSpinner />
            searching ..
          </Loading>
        )}

        {!isLoading && searchedMedium === "tv" && (
          <>
            <SearchGrid>
              {pageTVSelect()?.map(({ name, poster_path, overview, id }) => (
                <SearchResultCard
                  key={id}
                  id={id}
                  title={name}
                  overview={overview}
                  placeHolderImgURL={
                    poster_path
                      ? getPosterImagePath(poster_path)
                      : "/images/placeholder.png"
                  }
                />
              ))}
            </SearchGrid>
            <Pagination
              handleClick={handlePageSet}
              activePage={activePage}
              totalPages={pagesToDisplay}
            />
          </>
        )}
        {!isLoading && searchedMedium === "movie" && (
          <AnimatePresence exitBeforeEnter key={searchedMedium}>
            <SearchGrid>
              {pageMovieSelect()?.map(
                ({ title, poster_path, overview, id }) => (
                  <SearchResultCard
                    key={id}
                    id={id}
                    title={title}
                    overview={overview}
                    placeHolderImgURL={
                      poster_path
                        ? getPosterImagePath(poster_path)
                        : "/images/placeholder.png"
                    }
                  />
                )
              )}
            </SearchGrid>
            <Pagination
              handleClick={handlePageSet}
              activePage={activePage}
              totalPages={pagesToDisplay}
            />
          </AnimatePresence>
        )}
      </PageWrapper>
    </AnimatePresence>
  );
};

export default search;

const PageWrapper = styled(motion.div)`
  background: linear-gradient(
      180deg,
      rgba(16, 39, 79, 0.9668242296918768) 0%,
      rgba(16, 39, 79, 0.8855917366946778) 29%
    ),
    url(/images/movielist.png);
  background-size: 130%;
  background-position: center;

  height: 100%;
  display: flex;
  flex-direction: column;
`;
