// import App from 'next/app'
import { CacheProvider, Global, ThemeProvider } from '@emotion/react';
import { cache } from '@emotion/css';
import { Provider as ReduxStoreProvider } from 'react-redux';

import { AppProps } from 'next/app';
import { globalStyles } from '../shared/styles';
import { store } from '../redux/store';
import { theme } from '../theme/theme';
import Layout from '../layout';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

// see: https://nextjs.org/docs/advanced-features/custom-app
function MyApp({ Component, pageProps }: AppProps) {
    return (
        <ReduxStoreProvider store={store}>
            <ThemeProvider theme={theme}>
                <CacheProvider value={cache}>
                <Global styles={globalStyles} />
                <Layout>
                    <Component {...pageProps} />
                </Layout>
                </CacheProvider>
            </ThemeProvider>
        </ReduxStoreProvider>
    );
}

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async appContext => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext)

//   return { ...appProps }
// }

export default MyApp;
