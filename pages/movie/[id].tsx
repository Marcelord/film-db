import React from "react";
import { GetServerSideProps, GetServerSidePropsContext, NextPage } from "next";
import styled from "@emotion/styled";
import { AxiosError } from "axios";
import { motion, AnimatePresence } from "framer-motion";

import {
  getMovieById,
  getTrailers,
  getCredits,
  getRecommended,
} from "../../API_CALLS/movies.service";

import {
  MovieByIdResponse,
  CreditsResponse,
  IRecommendedResult,
} from "../../API_CALLS/movies.types";

import { pageSlideVariant } from "../../animations/pageSlide";

import DetailHero from "../../components/detailsComponents/DetailHero/DetailHero";
import DetailOverview from "../../components/detailsComponents/DetailOverview/DetailOverview";
import DetailRecommended from "../../components/detailsComponents/DetailRecommended/DetailRecommended";
import OrbitSpinner from "../../components/loader/OrbitSpinner";
import { PageWrapper, ErrorContainer } from "../../shared/detailByIdPagesStyle";

interface MovieDetailPageInitialProps {
  movieDetail: MovieByIdResponse;
  initialFetchError: AxiosError | null;
  trailerKey: string | null;
  credits: CreditsResponse | null;
  recommended: IRecommendedResult[] | null;
}

const MovieDetailPage: NextPage<MovieDetailPageInitialProps> = ({
  initialFetchError,
  movieDetail,
  trailerKey,
  credits,
  recommended,
}) => {
  return (
    <AnimatePresence initial={true} exitBeforeEnter>
      <PageWrapper
        variants={pageSlideVariant}
        initial="initial"
        animate="animate"
        exit="exit"
      >
        {initialFetchError && (
          <ErrorContainer>
            <OrbitSpinner size={160} />
            <h1>Something went wrong</h1>
            <h2>try to reload the page or different movie</h2>
          </ErrorContainer>
        )}

        {!initialFetchError && (
          <>
            <DetailHero
              backdrop_path={movieDetail.backdrop_path}
              title={movieDetail.title}
              genres={movieDetail.genres}
              release_date={movieDetail.release_date}
              poster_path={movieDetail.poster_path}
              popularity={movieDetail.popularity}
              vote_average={movieDetail.vote_average}
              trailerKey={trailerKey}
            />
            <DetailOverview
              budget={movieDetail.budget}
              revenue={movieDetail.revenue}
              overview={movieDetail.overview}
              credits={credits}
            />
            <DetailRecommended
              mediumType="movie"
              recommendations={recommended}
            />
          </>
        )}
      </PageWrapper>
    </AnimatePresence>
  );
};

export default MovieDetailPage;

export const getServerSideProps: GetServerSideProps<MovieDetailPageInitialProps> = async (
  context: GetServerSidePropsContext
) => {
  const { id } = context.params;
  let movieDetail: MovieByIdResponse;
  let initialFetchError: AxiosError | null;
  let trailerKey: string | null;
  let credits: CreditsResponse | null;
  let recommended: IRecommendedResult[];
  const idAsNumber = Number(id);

  try {
    const { data } = await getMovieById(idAsNumber);
    movieDetail = data;
    initialFetchError = null;
  } catch (error) {
    initialFetchError = error as AxiosError;
  }

  try {
    const { data } = await getTrailers("movie", idAsNumber);

    const trailer = data.results.find(
      (result) => result.type === "Trailer" && result.site === "YouTube"
    );

    trailerKey = trailer.key;
  } catch (trailerFetchError) {
    trailerKey = null;
  }

  try {
    const { data } = await getCredits("movie", idAsNumber);
    credits = data;
  } catch (creditsError) {
    credits = null;
  }

  try {
    const { data } = await getRecommended("movie", idAsNumber);
    recommended = data.results;
  } catch (recommendedError) {
    recommended = null;
  }

  return {
    props: {
      movieDetail,
      initialFetchError,
      trailerKey,
      credits,
      recommended,
    },
  };
};
