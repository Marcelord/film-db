import React from "react";
import styled from "@emotion/styled";
import Link from "next/link";

export default () => {
  return (
    <PageContainer>
      <StyledError>
        <p>... well something is missing</p>
        <h1>404</h1>
        <p>I bet you did typo in url</p>
        <Link href='/'>
          <StyledButton>GO BACK</StyledButton>
        </Link>
      </StyledError>
    </PageContainer>
  );
};

const StyledError = styled.div`
  padding: 2rem;
  /* border-top: 1px solid white; */
  border-right: 1px solid white;

  p {
    font-size: 1.5rem;
    margin: 0;
  }

  h1 {
    font-size: 6rem;
    margin: 0;
  }
`;

const PageContainer = styled.div`
  background: black;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;

const StyledButton = styled.button`
  margin-top: 2rem;
  padding: 0.8rem 3rem;
  background: transparent;
  color: white;
  border: 1px solid white;
  cursor: pointer;
  border-radius: 3px;
`;
