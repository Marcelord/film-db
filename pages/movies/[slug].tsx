import React, { useEffect } from "react";
import { AnimatePresence } from "framer-motion";
import { GetServerSideProps, GetServerSidePropsContext, NextPage } from "next";

import { useSelector, useDispatch } from "react-redux";
import { useRouter } from "next/router";

import { AllowedMoviesSlugs, baseRoutes } from "../../constants/Routes";

import { slugMovieCollections } from "../../API_CALLS/movies.service";
import { MoviesSlugs, MoviesResponse } from "../../API_CALLS/movies.types";
import { IReduxStore } from "../../redux/store";
import {
  fetchMovieCollectionSuccess,
  fetchMovieCollection,
} from "../../redux/collections/collections.actions";

import DetailHero from "../../components/detailsComponents/DetailHero/DetailHero";
import { pageSlideVariant } from "../../animations/pageSlide";
import CollectionCard from "../../components/cards/CollectionCard";
import OrbitSpinner from "../../components/loader/OrbitSpinner";
import Arrow from "../../components/icons/Arrow";

import { getPosterImagePath } from "../../utils/imageUtils";
import { getDate } from "../../utils/getDate";

import {
  ScrollToTop,
  Loading,
  AddButton,
  Grid,
  SlugPageWrapper,
} from "../../shared/slugPageStyles";
import { useWindowScrollCheck } from "../../hooks/useWindowScrollCehck";
import scrollOnTopPage from "../../utils/scrollOnTopPage";

interface MoviesSlugPageProps {
  initialCollection: MoviesResponse | null;
}

const MoviesSlugPage: NextPage<MoviesSlugPageProps> = ({
  initialCollection,
}) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { slug } = router.query;

  const { showScrollToTop } = useWindowScrollCheck();

  const { page, results } = useSelector(
    (state: IReduxStore) => state.collections.moviesCollection
  );

  const { isLoading } = useSelector((state: IReduxStore) => state.collections);

 
  const selection = results?.length > initialCollection.results.length  ? results :  initialCollection.results;

  useEffect(() => {
    if (initialCollection) {
      dispatch(fetchMovieCollectionSuccess(initialCollection));
    }
  }, []);

  return (
    <AnimatePresence initial={true} exitBeforeEnter>
      <SlugPageWrapper
        variants={pageSlideVariant}
        initial="initial"
        animate="animate"
        exit="exit"
      >
        {selection && (
          <>
            <DetailHero
              backdrop_path={selection[0].backdrop_path}
              poster_path={selection[0].poster_path}
              popularity={selection[0].popularity}
              genres={null}
              vote_average={selection[0].vote_average}
              trailerKey={null}
              key={selection[0].id}
              release_date={selection[0].release_date}
              title={selection[0].title}
              isCenteredPoster
              linkToDetails
              linkHandler={() => {
                router.push(`${baseRoutes.movie}/${selection[0].id}`);
              }}
            />
            <Grid>
              {selection.map((item, index) => {
                if (index > 0) {
                  return (
                    <CollectionCard
                      key={item.title + item.id + index}
                      imagePath={
                        item.poster_path
                          ? getPosterImagePath(item.poster_path)
                          : "/images/placeholder.png"
                      }
                      title={item.title}
                      rating={item.vote_average}
                      releaseDate={getDate(item.release_date)}
                      onSelect={() => {
                        router.push(`${baseRoutes.movie}/${item.id}`);
                      }}
                    />
                  );
                }
              })}
            </Grid>

            {!isLoading && (
              <AddButton
                whileHover={{ scale: 1.12 }}
                onClick={() =>
                  dispatch(fetchMovieCollection(page + 1, slug as string))
                }
              >
                see more
              </AddButton>
            )}

            {isLoading && (
              <Loading>
                <OrbitSpinner size={80} color={"gold"} />
                <p>loading</p>
              </Loading>
            )}
          </>
        )}

        <AnimatePresence>
          {showScrollToTop && (
            <ScrollToTop
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              onClick={scrollOnTopPage}
            >
              <Arrow />
              <p>to top</p>
            </ScrollToTop>
          )}
        </AnimatePresence>
      </SlugPageWrapper>
    </AnimatePresence>
  );
};

export default MoviesSlugPage;

export const getServerSideProps: GetServerSideProps<MoviesSlugPageProps> = async (
  context: GetServerSidePropsContext
) => {
  const { slug } = context.params;
  let initialCollection: MoviesResponse;

  slugMovieCollections(slug as MoviesSlugs);

  if (AllowedMoviesSlugs.includes(slug as string)) {
    try {
      const { data } = await slugMovieCollections(slug as MoviesSlugs);
      initialCollection = data;
    } catch (error) {
      initialCollection = null;
    }
  } else {
    // Route Guard checking if the options are allowed
    context.res.setHeader("location", "/");
    context.res.statusCode = 303;
    context.res.end();
    return;
  }

  return {
    props: {
      initialCollection,
    },
  };
};
