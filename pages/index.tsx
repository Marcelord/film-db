import { motion, AnimatePresence } from "framer-motion";
import { pageSlideVariant } from "../animations/pageSlide";
import HeroComponent from "../components/heroComponents/HeroComponent";
import HeroVideo from "../components/VideoComponents/HeroVideo";
import TrendingNow from "../components/TrendingNow/TrendingNow";
import { GetStaticProps, NextPage } from "next";
import { IMovieResult } from "../API_CALLS/movies.types";
import { getPopularMovies, getTrailers } from "../API_CALLS/movies.service";

interface HomeProps {
  initialHeroList: IMovieResult[];
  initialYouTubeKeys: string[];
}

const Home: NextPage<HomeProps> = ({ initialHeroList, initialYouTubeKeys }) => {
  return (
    <AnimatePresence initial={true} exitBeforeEnter>
      <motion.div
        variants={pageSlideVariant}
        initial="initial"
        animate="animate"
        exit="exit"
      >
        <HeroComponent initialHeroList={initialHeroList} />
        <HeroVideo initialTrailerKeys={initialYouTubeKeys} />
        <TrendingNow />
      </motion.div>
    </AnimatePresence>
  );
};

export default Home;

export const getStaticProps: GetStaticProps<HomeProps> = async () => {
  let initialHeroList: IMovieResult[];
  let initialYouTubeKeys: string[];

  try {
    const { data } = await getPopularMovies();

    initialHeroList = data.results;
  } catch (heroListError) {
    initialHeroList = [];
  }

  const youTubePromises = initialHeroList.map((result) =>
    getTrailers("movie", result.id)
  );

  try {
    const response = await Promise.all(youTubePromises);

    const datas = response.map(({ data }) =>
      data.results.find(
        (result) => result.type === "Trailer" && result.site === "YouTube"
      )
    );

    // for some reason you cant chain methods in get static props
    const results = datas.map((result) => result && result.key);
    const keys = results.filter((result) => result);

    initialYouTubeKeys = keys;
  } catch (youTubeError) {
    initialYouTubeKeys = [];
  }

  return {
    props: { initialHeroList, initialYouTubeKeys },
  };
};
