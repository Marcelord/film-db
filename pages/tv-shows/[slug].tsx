import React, { useEffect } from "react";
import { GetServerSideProps, GetServerSidePropsContext, NextPage } from "next";
import { useDispatch, useSelector } from "react-redux";
import { AnimatePresence } from "framer-motion";
import { useRouter } from "next/router";

import { slugTVCollections } from "../../API_CALLS/movies.service";

import scrollOnTopPage from "../../utils/scrollOnTopPage";
import { getPosterImagePath } from "../../utils/imageUtils";
import { getDate } from "../../utils/getDate";

import { AllowedTvsSlugs, baseRoutes } from "../../constants/Routes";

import { TVsSlugs, TVResponse } from "../../API_CALLS/movies.types";

import { useWindowScrollCheck } from "../../hooks/useWindowScrollCehck";

import { fadeOnMountVariants } from "../../animations/fadeOnMount";
import { pageSlideVariant } from "../../animations/pageSlide";

import {
  fetchTvCollectionSuccess,
  fetchTvCollection,
} from "../../redux/collections/collections.actions";

import DetailHero from "../../components/detailsComponents/DetailHero/DetailHero";
import CollectionCard from "../../components/cards/CollectionCard";
import OrbitSpinner from "../../components/loader/OrbitSpinner";
import Arrow from "../../components/icons/Arrow";
import { IReduxStore } from "../../redux/store";

import {
  Grid,
  AddButton,
  Loading,
  ScrollToTop,
  SlugPageWrapper,
} from "../../shared/slugPageStyles";

interface TvsSlugPageProps {
  initialCollection: TVResponse | null;
}

const TvsSlugPage: NextPage<TvsSlugPageProps> = ({ initialCollection }) => {
  const dispatch = useDispatch();
  const router = useRouter();
  const { slug } = router.query;

  const { showScrollToTop } = useWindowScrollCheck();

  const { page, results } = useSelector(
    (state: IReduxStore) => state.collections.tvCollection
  );

  const { isLoading } = useSelector((state: IReduxStore) => state.collections);

  const selection =
    results?.length > initialCollection.results.length
      ? results
      : initialCollection.results;

  useEffect(() => {
    // populate redux before pagination start
    if (initialCollection) {
      dispatch(fetchTvCollectionSuccess(initialCollection));
    }
  }, []);

  return (
    <AnimatePresence initial={true} exitBeforeEnter>
      <SlugPageWrapper
        variants={pageSlideVariant}
        initial="initial"
        animate="animate"
        exit="exit"
      >
        {selection && (
          <>
            <DetailHero
              backdrop_path={selection[0].backdrop_path}
              poster_path={selection[0].poster_path}
              popularity={selection[0].popularity}
              genres={null}
              vote_average={selection[0].vote_average}
              trailerKey={null}
              key={selection[0].id}
              first_air_date={selection[0].first_air_date}
              name={selection[0].name}
              isCenteredPoster
              linkToDetails
              linkHandler={() => {
                router.push(`${baseRoutes.tv}/${selection[0].id}`);
              }}
            />
            <Grid>
              {selection.map((item, index) => {
                if (index > 0) {
                  return (
                    <CollectionCard
                      key={item.name + item.id + index}
                      imagePath={
                        item.poster_path
                          ? getPosterImagePath(item.poster_path)
                          : "/images/placeholder.png"
                      }
                      title={item.name}
                      rating={item.vote_average}
                      releaseDate={getDate(item.first_air_date)}
                      onSelect={() => {
                        router.push(`${baseRoutes.tv}/${item.id}`);
                      }}
                    />
                  );
                }
              })}
            </Grid>

            {!isLoading && (
              <AddButton
                whileHover={{ scale: 1.12 }}
                onClick={() =>
                  dispatch(fetchTvCollection(page + 1, slug as string))
                }
              >
                see more
              </AddButton>
            )}

            {isLoading && (
              <Loading>
                <OrbitSpinner size={80} color={"gold"} />
                <p>loading</p>
              </Loading>
            )}
          </>
        )}

        <AnimatePresence>
          {showScrollToTop && (
            <ScrollToTop
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              exit={{ opacity: 0 }}
              onClick={scrollOnTopPage}
            >
              <Arrow />
              <p>to top</p>
            </ScrollToTop>
          )}
        </AnimatePresence>
      </SlugPageWrapper>
    </AnimatePresence>
  );
};

export default TvsSlugPage;

export const getServerSideProps: GetServerSideProps<TvsSlugPageProps> = async (
  context: GetServerSidePropsContext
) => {
  const { slug } = context.params;
  let initialCollection: TVResponse | null;

  if (AllowedTvsSlugs.includes(slug as string)) {
    try {
      const { data } = await slugTVCollections(slug as TVsSlugs);
      initialCollection = data;
    } catch (error) {
      initialCollection = null;
    }
  } else {
    // Route Guard checking if the options are allowed
    context.res.setHeader("location", "/");
    context.res.statusCode = 303;
    context.res.end();
    return;
  }

  return { props: { initialCollection } };
};
