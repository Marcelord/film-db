import React, { FC } from "react";

import { pageSlideVariant } from "../../animations/pageSlide";

import styled from "@emotion/styled";
import GridMenu from "../../components/grid/GridMenu";
import { MenuPageStyleWrapper } from "../../shared/styles";
import { tvShowsRoutes } from "../../constants/Routes";
import { AnimatePresence } from "framer-motion";

const TvShows: FC = () => {
  return (
    <AnimatePresence initial={true} exitBeforeEnter>
      <MenuPageStyleWrapper
        variants={pageSlideVariant}
        initial="initial"
        animate="animate"
        exit="exit"
      >
        <Container>
          <GridMenu
            backgroundUrl="/images/witcher.jpg"
            addressLinks={Object.values(tvShowsRoutes)}
          />
          <Heading>tv-shows</Heading>
        </Container>
      </MenuPageStyleWrapper>
    </AnimatePresence>
  );
};

export default TvShows;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10rem;
  justify-content: center;
  align-items: flex-start;
`;

const Heading = styled.h2`
  text-align: center;
  color: white;
  font-size: 2rem;
  position: relative;
  z-index: 10;
  text-transform: uppercase;
  width: 95%;
  max-width: 80rem;
  margin: 5rem auto;
  padding-top: 0.5rem;
  border-top: 1px solid white;
`;
