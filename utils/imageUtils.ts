import {
  secureImageUrl,
  posterSizes,
  backdropSizes,
} from "../constants/imageConstants"

export const getPosterImagePath = (poster: string | null) => {
  return poster && `${secureImageUrl}/${posterSizes[3]}${poster}`
}

export const getBackgroundWallpaperPath = (wallpaper: string | null) => {
  return wallpaper && `${secureImageUrl}/${backdropSizes[2]}${wallpaper}`
}
