export const getTitleFromUrl = (urlPath: string): string => {
  const lastSlashIndex = urlPath.lastIndexOf("/")
  const lastParam = urlPath.substr(lastSlashIndex+1)
  return lastParam.replace("-", " ")
}
