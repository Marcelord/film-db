export const getYouTubeThumbNail = (key: string) => {
  return `https://img.youtube.com/vi/${key}/maxres1.jpg`
}
