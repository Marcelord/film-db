import { Variants } from 'framer-motion';

export const popularProgressVariants = (
    initialColor: string,
    finalColor: string,
    popularity: number
): Variants => ({
    initial: {
        width: 0,
        background: initialColor,
    },
    animate: {
        width: `${popularity / 2 > 100 ? 100 : popularity / 2}%`,
        background: finalColor,
        transition: {
            delay: 1.2,
            type: 'spring',
            stiffness: 100,
            damping: 12,
        },
    },
});

export const voteAverageProgressVariants = (
    initialColor: string,
    finalColor: string,
    voteAverage: number
): Variants => ({
    initial: {
        width: 0,
        background: initialColor,
    },
    animate: {
        width: `${voteAverage * 10}%`,
        background: finalColor,
        transition: {
            delay: 1,
            type: 'spring',
            stiffness: 100,
            damping: 12,
        },
    },
});
