import { Variants } from 'framer-motion';

export const fadeOnMountVariants: Variants = {
    initial: {
        opacity: 0,
        y: 10,
    },

    animate: {
        opacity: 1,
        y: 0,
        transition: { type: 'tween', duration: 0.3, ease: 'easeInOut' },
    },

    exit: {
        opacity: 0,
        y: -50,
    },
};
