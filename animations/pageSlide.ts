import { Variants } from "framer-motion"

export const pageSlideVariant: Variants = {
  initial: {
    x: "-100vw",
  },
  animate: {
    x: 0,
    transition: {
      type: "spring",
      stiffness: 220,
      damping: 20,
    },
  },
  exit: {
    x: "100vw",
  },
}
