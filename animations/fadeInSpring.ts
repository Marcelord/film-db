import { Variants } from 'framer-motion';

export const fadeInSpringVariants: Variants = {
    initial: { opacity: 0, y: -10, x: 20 },
    animate: {
        opacity: 1,
        y: 0,
        x: 0,
        transition: { delay: 0.8, type: 'spring', damping: 12 },
    },
};
