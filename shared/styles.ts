import { motion } from 'framer-motion';
import styled from '@emotion/styled';
import { css } from '@emotion/core';

export const globalStyles = css`
    @import url('https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,700;1,500&family=Oxygen:wght@400;700&family=Roboto:wght@400;700&display=swap');
    html,
    body {
        margin: 0;
        padding: 0;
        height: 100%;
    }

    #__next {
        height: 100%;
        width: 100%;
    }

    a {
        text-decoration: none;
        color: inherit;
    }
    *,
    *::before,
    *::after {
        box-sizing: border-box;
    }
    html {
        font-size: 0.625rem;
    }
    body {
        font-size: 1.6rem;
        font-family: 'Roboto', Sans-serif;
        color: white;
    }

    button:active {
        outline: none;
        border: none;
    }

    button::-moz-focus-inner {
        outline: none;
        box-shadow: none;
    }

    button:focus {
        outline: 0;
        box-shadow: none;
    }
`;

export const MenuPageStyleWrapper = styled(motion.div)`
    position: relative;
    background-color: rgba(0, 0, 0, 1);
    display: flex;
    flex-direction: column;
    width: 100%;
    height: 100%;

    &::before {
        position: absolute;
        top: 0;
        left: 0;
        content: '';
        width: 100%;
        height: 100%;
        pointer-events: none;
        background: linear-gradient(0deg, rgba(6, 3, 15, 0.6), rgba(0, 0, 0, 1)),
            url('/images/starWars.jpg');
        background-size: cover;
        filter: blur(20px);
    }
`;
