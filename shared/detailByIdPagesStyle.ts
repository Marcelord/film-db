import styled from "@emotion/styled";
import { motion } from "framer-motion";

export const PageWrapper = styled(motion.div)`
  display: flex;
  flex-direction: column;
  height: 100%;
  background: black;
`;

export const ErrorContainer = styled.div`
  height: 60vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;
