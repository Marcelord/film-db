import styled from "@emotion/styled";
import { motion } from "framer-motion";

export const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(190px, 1fr));
  grid-gap: 10px;
  width: 80%;
  margin: 2rem auto;
`;

export const AddButton = styled(motion.button)`
  background: linear-gradient(180deg, #01b4e4, #737373);
  padding: 1rem 4.5rem;
  text-shadow: 0 1px 2px black;
  margin: 2rem auto;
  display: block;
  border: none;
  color: white;
  font-weight: 400;
  text-transform: uppercase;
  font-family: ${({ theme }) => theme.fonts.Oxygen};
  font-size: 1.4rem;
  cursor: pointer;
  box-shadow: 1px 6px 6px rgba(0,0,0, 0.3);
`;

export const Loading = styled(motion.div)`
  width: fit-content;
  margin: 2rem auto;
`;

export const ScrollToTop = styled(motion.div)`
  position: fixed;
  right: 20px;
  bottom: 50px;
  width: fit-content;
  height: 6rem;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  cursor: pointer;
  z-index: 50;

  svg {
    height: 90%;
    transform: rotate(-90deg);
    margin-bottom: 0.4rem;
  }
  p {
    margin: 0;
    text-align: center;
    text-transform: capitalize;
  }
`;

export const SlugPageWrapper = styled(motion.div)`
  display: flex;
  flex-direction: column;
  height: 100%;
  padding-bottom: 8rem;

  background: radial-gradient(
      circle,
      rgba(16, 39, 79, 0.407738095238095) 0%,
      rgba(16, 39, 79, 0.9511379551820728) 29%,
      rgba(0, 0, 0, 1) 77%
    ),
    url("/images/detailBcg.jpg");

  background-attachment: fixed;
`;
